package at.lyze.Console;

import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import at.lyze.Console.Everywhere.Creator;
import at.lyze.Console.Fa.FaBrowser;
import at.lyze.Console.Fa.FaCreator;
import at.lyze.Console.SoFurry.SoFurryBrowser;
import at.lyze.Console.SoFurry.SoFurryCreator;
import at.lyze.Console.Weasyl.WeasylBrowser;
import at.lyze.Console.Weasyl.WeasylCreator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by lyze on 06.06.15.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {


        System.out.println("~~~~~~~GENERAL INFORMATION~~~~~~~");

        System.out.println("[Fur Affinity] The Rating System is a bit weird on fa ... use the following things:");
        System.out.println("[Fur Affinity] General: 0");
        System.out.println("[Fur Affinity] Mature: 2");
        System.out.println("[Fur Affinity] Adult: 1");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("[So Furry] The 'Appropriate Audience' and 'Who Can View it' System is a bit weird on so furry... use the following things:");
        System.out.println("[So Furry]APPROPRIATE AUDIENCE");
        System.out.println("[So Furry]All Ages: 0");
        System.out.println("[So Furry]Adult: 1");
        System.out.println("[So Furry]Extreme: 2");

        System.out.println("\n[So Furry]WHO CAN VIEW IT");
        System.out.println("[So Furry]Public: 0");
        System.out.println("[So Furry]Group Only: 1");
        System.out.println("[So Furry]Friends Only: 2");
        System.out.println("[So Furry]Hidden: 3");

        System.out.println("\n[So Furry]You need to enter at least two tags");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        System.out.println("[WEASYL] You need to enter at least two tags");

        System.out.println("\n\n");

        if (!ConsoleHelper.yesNoQuestion("Understood the weird stuff on the sites?")) {
            System.out.printf("Well you need to :P ... exiting");
            return;
        }

        ConsoleHelper.ClearScreen();

        String faU = null;
        String faP = null;

        String soU = null;
        String soP = null;

        String weU = null;
        String weP = null;

        System.out.println("Trying to parse username / passwords from arguments");
        try {
            faU = args[0];
            System.out.println("Found FurAffinity Username");
            faP = args[1];
            System.out.println("Found FurAffinity Password");

            soU = args[2];
            System.out.println("Found so Furry Username");
            soP = args[3];
            System.out.println("Found so Furry Password");

            weU = args[4];
            System.out.println("Found weasyl Username");
            weP = args[5];
            System.out.println("Found weasyl Password");
        } catch (Exception e) {

        }

        if (faU == null) {
            faU = ConsoleHelper.getString("FurAffinity Username");
        }
        if (faP == null) {
            faP = ConsoleHelper.getString("FurAffinity Password");
        }

        if (soU == null) {
            soU = ConsoleHelper.getString("So Furry Username");
        }
        if (soP == null) {
            soP = ConsoleHelper.getString("So Furry Password");
        }

        if (weU == null) {
            weU = ConsoleHelper.getString("weasyl Username");
        }
        if (weP == null) {
            weP = ConsoleHelper.getString("weasyl Password");
        }

        HashMap<File, Artwork> elements = new HashMap<>();

        ArrayList<Browser> browserList = new ArrayList<>();
        ArrayList<Creator> creatorList = new ArrayList<>();

        //Initializes browsers
        browserList.add(new FaBrowser());
        browserList.add(new SoFurryBrowser());
        browserList.add(new WeasylBrowser());

        // Add every creator
        creatorList.add(new FaCreator("elements", browserList.get(0)));
        creatorList.add(new SoFurryCreator("elements", browserList.get(1)));
        creatorList.add(new WeasylCreator("elements", browserList.get(2)));

        for (int i = browserList.size() - 1; i >= 0; i--) {
            if (!ConsoleHelper.yesNoQuestion("Do you want to upload the images to " + browserList.get(i).name)) {
                creatorList.remove(i);
                browserList.remove(i);
            }
        }

        if (creatorList.isEmpty()) {
            System.out.println("No other Website is available to upload ... exiting");
            return;
        }

        // Fetch all Elements
        elements = creatorList.get(0).fetchElements();

        if (elements.size() == 0) {
            System.out.println("Found no files ... exiting");
            System.exit(0);
        }


        // Login to each browser
        for (Browser b : browserList) {

            System.out.println("Logging in ... " + b.name);

            String username = "";
            String password = "";

            if (b.name.equals("FurAffinity")) {
                username = faU;
                password = faP;
            } else if (b.name.equals("soFurry")) {
                username = soU;
                password = soP;
            } else if (b.name.equals("weasyl")) {
                username = weU;
                password = weP;
            } else {
                System.out.println("LYZE IS A IDIOT! " + b.name);
            }

            if (!b.login(username, password)) {
                System.out.println("Could not login to the page .... please provide a proper username and password");

                ConsoleHelper.ClearScreen();
            }
        }

        // Fetches all options for each site
        browserList.forEach(b -> b.fetchOptions());

        // ask the user each file

        System.out.println("PLEASE PROVIDE INFORMATIONS FOR <GLOBAL INFORMATIONS>");
        for (File f : elements.keySet()) {
            Artwork a = new Artwork();
            try {
                Creator.fillDefaults(f, a);
            } catch (IOException e) {
                e.printStackTrace();
            }
            elements.put(f, a);
        }

        for (Creator c : creatorList) {
            System.out.println("PLEASE PROVIDE INFORMATIONS FOR " + c.browser.name);

            for (File f : elements.keySet()) {
                try {
                    c.askUser(f, elements.get(f));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            ConsoleHelper.ClearScreen();
        }

        Set<Artwork> error = new HashSet<>();

        for (Creator c : creatorList) {

            System.out.println("WEBSITE CHANGED TO: " + c.browser.name);

            for (Artwork art : elements.values()) {
                System.out.println("~~~~~~~~~~~~~~~~~~");
                System.out.println("Uploading Image: " + art);
                System.out.println("~~~~~~~~~~~~~~~~~~");
                if (c.browser.upload(art)) {
                    System.out.println("Successfully uploaded image");
                } else {
                    System.err.println("Could not upload image");
                    error.add(art);
                }

                for (int i = c.cooldown; i > 0; i--) {
                    System.out.println("[Cooldown] " + i + " sek");
                    Thread.sleep(1000);
                }
            }

            c.browser.destroy();

            ConsoleHelper.ClearScreen();
        }

        ConsoleHelper.ClearScreen();

        System.out.println("~~~~~Upload Finished~~~~~");

        if (error.size() > 0) {
            System.out.println("The following files couldn't be uploaded");
            for (Artwork a : error) {
                System.out.println(a.title + "(" + new File(a.path).getName() + ")");
            }

            if (ConsoleHelper.yesNoQuestion("Do you want to move them to the 'error' folder")) {
                File errorDir = new File("error");
                if (!errorDir.exists()) {
                    errorDir.mkdirs();
                }

                if (!errorDir.isDirectory()) {
                    System.out.println("The directory is no directory! Please delete the file called 'error'");
                    return;
                }

                for (Artwork a : error) {
                    File f = new File(a.path);
                    System.out.println("Moving " + f.getName());
                    f.renameTo(new File(errorDir + "/" + f.getName()));
                }
            }
        }

        ConsoleHelper.ClearScreen();

        System.out.println("~~~~~~~~~Finished (created by www.lyze.at)~~~~~~~~~");
    }

}
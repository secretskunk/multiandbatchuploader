package at.lyze.Console;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by lyze on 22.07.15.
 */
public class ImageFileFilter implements FileFilter {
    private final String[] okFileExtensions =
            new String[]{"jpg", "png", "gif", "jpeg", "bmp"};

    public boolean accept(File file) {
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}
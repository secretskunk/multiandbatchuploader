package at.lyze.Console;

import java.util.Scanner;

/**
 * Created by lyze on 25.07.15.
 */
public class ConsoleHelper {


    public static String getString(String message) {
        System.out.print(message + ": ");

        return getInput();
    }

    public static boolean yesNoQuestion(String question) {
        boolean correct;
        boolean value = true;

        do {
            correct = true;

            System.out.print(question + " (y/n)? ");

            String elem = getInput();

            if (elem.equals("y") || elem.equals("yes")) {
                correct = true;
                value = true;
            } else if (elem.equals("n") || elem.equals("no")) {
                correct = true;
                value = false;
            } else {
                correct = false;
            }


        } while (!correct);

        return value;
    }

    public static String getInput() {
        Scanner scanner = new Scanner(System.in);
        String ret = scanner.nextLine();

        return ret;
    }

    public static void ClearScreen() {
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
    }

}

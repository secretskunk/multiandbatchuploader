package at.lyze.Console.SoFurry;

/**
 * Created by lyze on 25.07.15.
 */
public class SoFurryArtwork {
    public String folder;
    public String audienceAppropriate;
    public String whoCanSee;

    @Override
    public String toString() {
        return "SoFurryArtwork{" +
                "folder='" + folder + '\'' +
                ", audienceAppropriate=" + audienceAppropriate +
                ", whoCanSee=" + whoCanSee +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SoFurryArtwork)) return false;

        SoFurryArtwork that = (SoFurryArtwork) o;

        if (folder != null ? !folder.equals(that.folder) : that.folder != null) return false;
        if (audienceAppropriate != null ? !audienceAppropriate.equals(that.audienceAppropriate) : that.audienceAppropriate != null)
            return false;
        return !(whoCanSee != null ? !whoCanSee.equals(that.whoCanSee) : that.whoCanSee != null);

    }

    @Override
    public int hashCode() {
        int result = folder != null ? folder.hashCode() : 0;
        result = 31 * result + (audienceAppropriate != null ? audienceAppropriate.hashCode() : 0);
        result = 31 * result + (whoCanSee != null ? whoCanSee.hashCode() : 0);
        return result;
    }
}

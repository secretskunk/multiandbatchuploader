package at.lyze.Console.SoFurry;

import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

/**
 * Created by lyze on 25.07.15.
 */
public class SoFurryBrowser extends Browser {

    public SoFurryBrowser() {
        super("soFurry");
    }

    @Override
    public boolean login(String user, String password) {
        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/user/login");

            HtmlForm loginForm = page.getForms().get(1);

            HtmlTextInput username = loginForm.getInputByName("LoginForm[sfLoginUsername]");
            username.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("LoginForm[sfLoginPassword]");
            pass.setText(password);

            HtmlSubmitInput submitInput = loginForm.getInputByValue("Login");
            page = submitInput.click();

            if (page.asText().contains("Please fix the following input errors")) {
                System.err.println("Could not login: " + page.asXml());
                return false;
            } else {
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {

        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/upload/details?contentType=1");

            HtmlForm uploadForm = (HtmlForm) page.getElementById("sf_uploadform");

            HtmlTextInput title = uploadForm.getInputByName("UploadForm[P_title]");
            title.setText(artwork.title);

            HtmlFileInput fileInput = (HtmlFileInput) page.getElementById("UploadForm_binarycontent");
            fileInput.setValueAttribute(artwork.path);

            HtmlSelect folder = uploadForm.getSelectByName("UploadForm[folderId]");
            boolean set = false;
            for (HtmlOption option : folder.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.so.folder.toLowerCase())) {
                    option.setSelected(true);
                    set = true;
                    break;
                }
            }
            if (!set) {
                HtmlTextInput newfolder = uploadForm.getInputByName("UploadForm[newFolderName]");
                newfolder.setText(artwork.so.folder);
            }

            List<HtmlInput> ratings = uploadForm.getInputsByName("UploadForm[contentLevel]");
            for (HtmlInput rating : ratings) {
                if (rating.getValueAttribute().trim().equals(artwork.so.audienceAppropriate)) {
                    rating.setChecked(true);
                }
            }

            List<HtmlInput> whoCanSee = uploadForm.getInputsByName("UploadForm[P_hidePublic]");
            for (HtmlInput who : whoCanSee) {
                if (who.getValueAttribute().trim().toLowerCase().equals(artwork.so.whoCanSee)) {
                    who.setChecked(true);
                }
            }

            HtmlTextArea description = uploadForm.getTextAreaByName("UploadForm[description]");
            description.setText(artwork.comment);

            HtmlTextInput tags = uploadForm.getInputByName("UploadForm[formtags]");
            tags.setText(artwork.tag.replace(" ", ", "));

            HtmlSubmitInput upload = uploadForm.getInputByName("yt0");
            page = upload.click();

            if (page.getUrl().toString().contains("view")) {
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " ): " + page.asXml());
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/upload/details?contentType=1");

            HtmlForm uploadForm = (HtmlForm) page.getElementById("sf_uploadform");

            HtmlSelect folder = uploadForm.getSelectByName("UploadForm[folderId]");

            options.put("so_folder", new HashSet<>());
            for (HtmlOption option : folder.getOptions()) {
                options.get("so_folder").add(option.asText().toLowerCase());
            }
            options.get("so_folder").add("");




        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

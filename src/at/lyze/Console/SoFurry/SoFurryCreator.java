package at.lyze.Console.SoFurry;

import at.lyze.Console.ConsoleHelper;
import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import at.lyze.Console.Everywhere.Creator;

import java.io.File;
import java.io.IOException;

/**
 * Created by lyze on 25.07.15.
 */
public class SoFurryCreator extends Creator {
    public SoFurryCreator(String inputDir, Browser browser) {
        super(inputDir, browser, 5);
    }

    @Override
    public void askUser(File file, Artwork artwork) throws IOException {
        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n" + file.getName());

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n");

        artwork.so.folder = ConsoleHelper.getString("Folder");

        String tmp = "awdiouuawd";
        while (!tmp.equals("1") && !tmp.equals("2") && !tmp.equals("0"))
            tmp = ConsoleHelper.getString("Appropriate Audience");
        artwork.so.audienceAppropriate = tmp;

        tmp = "awdiouuawd";
        while (!tmp.equals("1") && !tmp.equals("2") && !tmp.equals("0") && !tmp.equals("3"))
            tmp = ConsoleHelper.getString("Who can view it");
        artwork.so.whoCanSee = tmp;
    }
}

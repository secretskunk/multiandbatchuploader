package at.lyze.Console.Everywhere;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by lyze on 22.07.15.
 */
public abstract class Browser {
    public HashMap<String, Set<String>> options;
    public String name;
    protected WebClient client;

    public Browser(String name) {
        client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setJavaScriptEnabled(false);
        options = new HashMap<>();

        this.name = name;
    }

    public abstract boolean login(String user, String password);

    public abstract boolean upload(Artwork artwork);

    public void destroy() {
        client.close();
    }

    public abstract void fetchOptions();
}

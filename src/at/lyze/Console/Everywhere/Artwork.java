package at.lyze.Console.Everywhere;

import at.lyze.Console.Fa.FaArtwork;
import at.lyze.Console.SoFurry.SoFurryArtwork;
import at.lyze.Console.Weasyl.WeasylArtwork;

/**
 * Created by lyze on 22.07.15.
 */
public class Artwork {
    public String title;
    public String comment;
    public String tag;
    public String path;

    public FaArtwork fa = new FaArtwork();
    public SoFurryArtwork so = new SoFurryArtwork();
    public WeasylArtwork we = new WeasylArtwork();

    public Artwork() {

    }

    public Artwork(String title, String comment, String tag, String path) {
        this.title = title;
        this.comment = comment;
        this.tag = tag;
        this.path = path;
    }

    @Override
    public String toString() {
        return "Artwork{" +
                "title='" + title + '\'' +
                ", comment='" + comment + '\'' +
                ", tag='" + tag + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Artwork)) return false;

        Artwork artwork = (Artwork) o;

        if (title != null ? !title.equals(artwork.title) : artwork.title != null) return false;
        if (comment != null ? !comment.equals(artwork.comment) : artwork.comment != null) return false;
        if (tag != null ? !tag.equals(artwork.tag) : artwork.tag != null) return false;
        if (path != null ? !path.equals(artwork.path) : artwork.path != null) return false;
        if (fa != null ? !fa.equals(artwork.fa) : artwork.fa != null) return false;
        if (so != null ? !so.equals(artwork.so) : artwork.so != null) return false;
        return !(we != null ? !we.equals(artwork.we) : artwork.we != null);

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (fa != null ? fa.hashCode() : 0);
        result = 31 * result + (so != null ? so.hashCode() : 0);
        result = 31 * result + (we != null ? we.hashCode() : 0);
        return result;
    }
}

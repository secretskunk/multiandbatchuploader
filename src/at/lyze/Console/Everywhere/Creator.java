package at.lyze.Console.Everywhere;

import at.lyze.Console.ConsoleHelper;
import at.lyze.Console.ImageFileFilter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by lyze on 22.07.15.
 */
public abstract class Creator {

    public Browser browser;
    public int cooldown;
    protected File inputDir;

    public Creator(String inputDir, Browser browser, int cooldown) {
        this.inputDir = new File(inputDir);
        this.browser = browser;
        this.cooldown = cooldown;

        if (!this.inputDir.exists()) {
            if (ConsoleHelper.yesNoQuestion("Looks like it's the first time you use this program. Wanna create everything :)")) {
                this.inputDir.mkdirs();
            } else {
                System.out.println("Good bye!");
                System.exit(0);
            }

            System.out.println("Please look into the folder and put images in there!");
            System.exit(0);
        }
    }

    public static void fillDefaults(File file, Artwork art) throws IOException {

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n" + file.getName());

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println();

        art.title = ConsoleHelper.getString("Title");
        art.comment = ConsoleHelper.getString("Comment");
        art.tag = ConsoleHelper.getString("Tag [Seperated with ' ']");
        art.path = file.getCanonicalPath();
    }

    public void askUserAllTheFiles(HashMap<File, Artwork> elements) {
        for (File file : elements.keySet()) {
            try {
                askUser(file, elements.get(file));
            } catch (IOException e) {
                System.err.println("Error in file: " + file);
                e.printStackTrace();
            }
        }
    }

    public abstract void askUser(File file, Artwork artwork) throws IOException;

    public HashMap<File, Artwork> fetchElements() {
        HashMap<File, Artwork> elements = new HashMap<>();
        if (!inputDir.isDirectory()) {
            System.out.println(inputDir + " not a inputDir!");
            return null;
        }

        System.out.println("Searching for files ...");

        for (File f : inputDir.listFiles(new ImageFileFilter())) {
            if (f.isDirectory())
                continue;

            System.out.println("Found Image: " + f.getName());
            elements.put(f, null);
        }

        System.out.println("Found " + elements.size() + " Files");

        return elements;
    }
}

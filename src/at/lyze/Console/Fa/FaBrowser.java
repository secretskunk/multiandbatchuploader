package at.lyze.Console.Fa;

import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

/**
 * Created by lyze on 22.07.15.
 */
public class FaBrowser extends Browser {

    public FaBrowser() {
        super("FurAffinity");
    }

    @Override
    public boolean login(String user, String password) {
        try {
            HtmlPage page = client.getPage("https://www.furaffinity.net/login/");
            HtmlForm loginForm = page.getForms().get(0);

            HtmlTextInput name = loginForm.getInputByName("name");
            name.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("pass");
            pass.setText(password);

            HtmlSubmitInput loginButton = loginForm.getInputByName("login");
            page = loginButton.click();

            if (page.asText().contains("You have typed in an erroneous username or password, please try again...")) {
                System.err.println("Could not login: " + page.asXml());
                return false;
            } else
                return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {

        try {
            HtmlPage page = client.getPage("http://www.furaffinity.net/submit/");

            HtmlForm type = page.getFormByName("myform");
            List<HtmlInput> submissionType = type.getInputsByName("submission_type");

//            for (HtmlInput input : submissionType) {
//                System.out.print(input.getAttribute("value") + ": ");
//                if (input instanceof HtmlRadioButtonInput) {
//                    System.out.println(input.isChecked());
//                }
//            }

            page = type.getInputByValue("Next Step").click();

            HtmlForm upload = page.getFormByName("myform");
            HtmlFileInput fileUpload = upload.getInputByName("submission");
            fileUpload.setValueAttribute(artwork.path);

            page = upload.getInputByValue("Next").click();

            HtmlForm settings = page.getFormByName("myform");
            HtmlSelect category = settings.getSelectByName("cat");
            for (HtmlOption option : category.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.submissionCategory.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect theme = settings.getSelectByName("atype");
            for (HtmlOption option : theme.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.submissionTheme.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect species = settings.getSelectByName("species");
            for (HtmlOption option : species.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.species.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect gender = settings.getSelectByName("gender");
            for (HtmlOption option : gender.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.gender.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            List<HtmlInput> ratings = settings.getInputsByName("rating");
            for (HtmlInput rating : ratings) {
                if (rating.getValueAttribute().trim().toLowerCase().equals(artwork.fa.submissionRating.toLowerCase())) {
                    rating.setChecked(true);
                }
            }

            HtmlTextInput title = settings.getInputByName("title");
            title.setText(artwork.title);

            HtmlTextArea message = settings.getTextAreaByName("message");
            message.setText(artwork.comment);

            HtmlTextArea keywords = settings.getTextAreaByName("keywords");
            keywords.setText(artwork.tag);

            HtmlCheckBoxInput scrap = settings.getInputByName("scrap");
            scrap.setChecked(artwork.fa.scraps);

            page = settings.getInputByName("submit").click();
            if (page.getUrl().toString().contains("/view/")) {
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " ): " + page.asXml());
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void destroy() {
        client.close();
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("http://www.furaffinity.net/submit/");

            HtmlForm type = page.getFormByName("myform");
            List<HtmlInput> submissionType = type.getInputsByName("submission_type");

//            for (HtmlInput input : submissionType) {
//                System.out.print(input.getAttribute("value") + ": ");
//                if (input instanceof HtmlRadioButtonInput) {
//                    System.out.println(input.isChecked());
//                }
//            }

            page = type.getInputByValue("Next Step").click();

            HtmlForm upload = page.getFormByName("myform");
            HtmlFileInput fileUpload = upload.getInputByName("submission");
            fileUpload.setValueAttribute(new File("data/tmp.png").getCanonicalPath());

            page = upload.getInputByValue("Next").click();


            HtmlForm settings = page.getFormByName("myform");


            HtmlSelect category = settings.getSelectByName("cat");
            options.put("fa_cat", new HashSet<>());
            for (HtmlOption option : category.getOptions()) {
                options.get("fa_cat").add(option.asText().toLowerCase());
            }
            options.get("fa_cat").add("");

            HtmlSelect theme = settings.getSelectByName("atype");
            options.put("fa_atype", new HashSet<>());
            for (HtmlOption option : theme.getOptions()) {
                options.get("fa_atype").add(option.asText().toLowerCase());
            }
            options.get("fa_atype").add("");

            HtmlSelect species = settings.getSelectByName("species");
            options.put("fa_species", new HashSet<>());
            for (HtmlOption option : species.getOptions()) {
                options.get("fa_species").add(option.asText().toLowerCase());
            }
            options.get("fa_species").add("");

            HtmlSelect gender = settings.getSelectByName("gender");
            options.put("fa_gender", new HashSet<>());
            for (HtmlOption option : gender.getOptions()) {
                options.get("fa_gender").add(option.asText().toLowerCase());
            }
            options.get("fa_gender").add("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

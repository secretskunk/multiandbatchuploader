package at.lyze.Console.Fa;

/**
 * Created by lyze on 22.07.15.
 */
public class FaArtwork {
    public String submissionCategory;
    public String submissionTheme;
    public String species;
    public String gender;
    public String submissionRating;
    public boolean scraps;

    public FaArtwork() {
        super();
    }

    @Override
    public String toString() {
        return "FaArtwork{" +
                "submissionCategory='" + submissionCategory + '\'' +
                ", submissionTheme='" + submissionTheme + '\'' +
                ", species='" + species + '\'' +
                ", gender='" + gender + '\'' +
                ", submissionRating='" + submissionRating + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FaArtwork)) return false;

        FaArtwork faArtwork = (FaArtwork) o;

        if (submissionCategory != null ? !submissionCategory.equals(faArtwork.submissionCategory) : faArtwork.submissionCategory != null)
            return false;
        if (submissionTheme != null ? !submissionTheme.equals(faArtwork.submissionTheme) : faArtwork.submissionTheme != null)
            return false;
        if (species != null ? !species.equals(faArtwork.species) : faArtwork.species != null) return false;
        if (gender != null ? !gender.equals(faArtwork.gender) : faArtwork.gender != null) return false;
        return !(submissionRating != null ? !submissionRating.equals(faArtwork.submissionRating) : faArtwork.submissionRating != null);

    }

    @Override
    public int hashCode() {
        int result = submissionCategory != null ? submissionCategory.hashCode() : 0;
        result = 31 * result + (submissionTheme != null ? submissionTheme.hashCode() : 0);
        result = 31 * result + (species != null ? species.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (submissionRating != null ? submissionRating.hashCode() : 0);
        return result;
    }
}

package at.lyze.Console.Fa;

import at.lyze.Console.ConsoleHelper;
import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import at.lyze.Console.Everywhere.Creator;

import java.io.File;
import java.io.IOException;

/**
 * Created by lyze on 22.07.15.
 */
public class FaCreator extends Creator {

    public FaCreator(String inputDir, Browser browser) {
        super(inputDir, browser, 120);
    }

    public void askUser(File file, Artwork artwork) throws IOException {

        FaBrowser faBrowser = (FaBrowser) browser;

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n" + file.getName());

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n");

        String tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";

        while (!faBrowser.options.get("fa_cat").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Submission Category");
        }
        artwork.fa.submissionCategory = tmp;
        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";

        while (!faBrowser.options.get("fa_atype").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Submission Theme");
        }
        artwork.fa.submissionTheme = tmp;
        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";

        while (!faBrowser.options.get("fa_species").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Species");
        }
        artwork.fa.species = tmp;
        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";

        while (!faBrowser.options.get("fa_gender").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Gender");
        }
        artwork.fa.gender = tmp;
        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";

        while (!tmp.equals("1") && !tmp.equals("2") && !tmp.equals("0"))
            tmp = ConsoleHelper.getString("Submission Rating");
        artwork.fa.submissionRating = tmp;

        artwork.fa.scraps = ConsoleHelper.yesNoQuestion("Is it a scrap");
    }
}

package at.lyze.Console.Weasyl;

import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.util.HashSet;

/**
 * Created by lyze on 25.07.15.
 */
public class WeasylBrowser extends Browser {

    public WeasylBrowser() {
        super("weasyl");
    }

    @Override
    public boolean login(String user, String password) {
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/signin");

            HtmlForm loginForm = page.getFormByName("signin");

            HtmlTextInput username = loginForm.getInputByName("username");
            username.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("password");
            pass.setText(password);

            HtmlButton submit = (HtmlButton) loginForm.getByXPath("//*[@id=\"login-box\"]/form/button").get(0);
            page = submit.click();

            if (page.asText().contains("The login information you entered was not correct.")) {
                System.err.println("Could not login: " + page.asXml());
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/submit/visual");

            HtmlForm uploadForm = page.getFormByName("submitvisual");

            HtmlFileInput upload = uploadForm.getInputByName("submitfile");
            upload.setValueAttribute(artwork.path);

            HtmlTextInput title = uploadForm.getInputByName("title");
            title.setText(artwork.title);

            HtmlSelect category = uploadForm.getSelectByName("subtype");
            for (HtmlOption option : category.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.subcategory.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect folder = uploadForm.getSelectByName("folderid");
            for (HtmlOption option : folder.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.folder.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect rating = uploadForm.getSelectByName("rating");
            for (HtmlOption option : rating.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.rating.toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlTextArea description = uploadForm.getTextAreaByName("content");
            description.setText(artwork.comment);

            HtmlTextArea tags = uploadForm.getTextAreaByName("tags");
            tags.setText(artwork.tag.replace(" ", ", "));

            HtmlButton submit = (HtmlButton) page.getByXPath("//*[@id=\"submit-form\"]/div[6]/button").get(0);

            page = submit.click();

            if (page.getUrl().toString().contains("submission")) {
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " ): " + page.asXml());
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/submit/visual");

            HtmlForm uploadForm = page.getFormByName("submitvisual");

            HtmlSelect category = uploadForm.getSelectByName("subtype");
            options.put("we_category", new HashSet<>());
            for (HtmlOption option : category.getOptions()) {
                options.get("we_category").add(option.asText().toLowerCase());
            }
            options.get("we_category").remove("");

            HtmlSelect folder = uploadForm.getSelectByName("folderid");
            options.put("we_folderid", new HashSet<>());
            for (HtmlOption option : folder.getOptions()) {
                options.get("we_folderid").add(option.asText().toLowerCase());
            }
            options.get("we_folderid").add("");

            HtmlSelect rating = uploadForm.getSelectByName("rating");
            options.put("we_rating", new HashSet<>());
            for (HtmlOption option : rating.getOptions()) {
                options.get("we_rating").add(option.asText().toLowerCase());
            }
            options.get("we_rating").remove("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

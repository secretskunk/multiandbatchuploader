package at.lyze.Console.Weasyl;

/**
 * Created by lyze on 25.07.15.
 */
public class WeasylArtwork {
    public String subcategory;
    public String folder;
    public String rating;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeasylArtwork)) return false;

        WeasylArtwork that = (WeasylArtwork) o;

        if (subcategory != null ? !subcategory.equals(that.subcategory) : that.subcategory != null) return false;
        if (folder != null ? !folder.equals(that.folder) : that.folder != null) return false;
        return !(rating != null ? !rating.equals(that.rating) : that.rating != null);

    }

    @Override
    public int hashCode() {
        int result = subcategory != null ? subcategory.hashCode() : 0;
        result = 31 * result + (folder != null ? folder.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }
}

package at.lyze.Console.Weasyl;

import at.lyze.Console.ConsoleHelper;
import at.lyze.Console.Everywhere.Artwork;
import at.lyze.Console.Everywhere.Browser;
import at.lyze.Console.Everywhere.Creator;

import java.io.File;
import java.io.IOException;

/**
 * Created by lyze on 25.07.15.
 */
public class WeasylCreator extends Creator {

    public WeasylCreator(String inputDir, Browser browser) {
        super(inputDir, browser, 5);
    }

    @Override
    public void askUser(File file, Artwork artwork) throws IOException {
        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n" + file.getName());

        for (int i = 0; i < file.getName().length(); i++)
            System.out.print("~");

        System.out.println("\n");

        String tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";
        while (!browser.options.get("we_category").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Category");
        }
        artwork.we.subcategory = tmp;

        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";
        while (!browser.options.get("we_folderid").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Folder");
        }
        artwork.we.folder = tmp;

        tmp = "awkljdhlksejrgjhohütrdptrdpihbtbg e540zgt384hrt";
        while (!browser.options.get("we_rating").contains(tmp.toLowerCase())) {
            tmp = ConsoleHelper.getString("Rating");
        }
        artwork.we.rating = tmp;

    }
}

package at.lyze.gui;

import at.lyze.gui.websites.everywhere.HardcodedArtworkSettings;
import at.lyze.gui.websites.everywhere.LoginStatus;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.MainWindow;

import javax.swing.*;
import java.util.HashMap;
import java.util.prefs.Preferences;

import static at.lyze.gui.util.Encryption.*;

/**
 * Created by lyze on 18.09.15.
 */
public class Main {
    public static void main(String[] args) {
      //set up prefences folder:
        try{
        Preferences.userRoot().getBoolean("setup", false);
        }  catch (NullPointerException e){
            Preferences.userRoot().putBoolean("setup", true);
            //init all prefs
            Preferences.userRoot().putBoolean("rememberUserInfo", true);
            Preferences.userRoot().putBoolean("userInfoPresent", false);
            Preferences.userRoot().put("faAppendString", "");
            Preferences.userRoot().put("faPrependString", "");
            Preferences.userRoot().put("sfPrependString", "");
            Preferences.userRoot().put("sfAppendString", "");
            Preferences.userRoot().put("wyPrependString", "");
            Preferences.userRoot().put("wyAppendString", "");
        }
        if(Preferences.userRoot().getBoolean("userInfoPresent", false)){
            if (!getMasterPassword()){
                //clear user data, no password
                Preferences.userRoot().putBoolean("rememberUserInfo", false);
                Preferences.userRoot().putBoolean("userInfoPresent", false);
                //just in case
                resetMasterPassword();

            }
        }

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.SEVERE);

        HashMap<String, String> argPairs = getArgPairs(args);
        FaBrowser faBrowser = new FaBrowser();
        SoFurryBrowser soFurryBrowser = new SoFurryBrowser();
        WeasylBrowser weasylBrowser = new WeasylBrowser();

        MainWindow win = new MainWindow(faBrowser, soFurryBrowser, weasylBrowser);
        win.init();
        win.displayLoginPrompt(faBrowser, soFurryBrowser, weasylBrowser, argPairs);






        ProgressMonitor monitor = new ProgressMonitor(win, "Fetching Options", "", 0, getBooleanCount(LoginStatus.FA, LoginStatus.SO, LoginStatus.WE));
        int progress = 0;
        if (LoginStatus.FA) {
            try {
                System.out.println("Fetching FA Options");
                HardcodedArtworkSettings.fa_submissionRating.remove("Couldn't login");
                faBrowser.fetchOptions();
                monitor.setProgress(++progress);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Can't fetch important files from FA. Make sure to use the beta!");
                LoginStatus.FA = false;
            }
        }
        if (LoginStatus.SO) {
            System.out.println("Fetching So Furry Options");
            HardcodedArtworkSettings.so_visibility.remove("Couldn't login");
            HardcodedArtworkSettings.so_audience.remove("Couldn't login");
            soFurryBrowser.fetchOptions();
            monitor.setProgress(++progress);
        }
        if (LoginStatus.WE) {
            System.out.println("Fetching Weasyl. Options");
            HardcodedArtworkSettings.fa_submissionRating.remove("Couldn't login");
            weasylBrowser.fetchOptions();
            monitor.setProgress(++progress);
        }

        win.display();
    }

    public static HashMap<String, String> getArgPairs(String[] args) {
        HashMap<String, String> pairs = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                pairs.put(args[i], args[i+1]);
            }
        }

        return pairs;
    }

    public static int getBooleanCount(boolean... vars) {
        int count = 0;
        for (boolean var : vars) {
            count += (var ? 1 : 0);
        }
        return count;
    }
}

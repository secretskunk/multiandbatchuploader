
package at.lyze.gui.util;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.prefs.Preferences;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.swing.*;

/**
 * Created by SECRETSKUNK on 12/29/15.
 */

public class Encryption {


    private static final String passwordCheck = "This_Is_A_Password_Check";
    private static byte[] masterPassword = null;
    // some code taken from www.owasp.org/index.php/Hashing_Java
    public static byte[] hash(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.reset();
        try {
            return digest.digest((password+"lolthisisASaLtHAHA1337").getBytes("UTF-8"));
        }catch (java.io.UnsupportedEncodingException e){
        }
        return null;
    }
    public static void resetMasterPassword(){
        masterPassword = null;
    }
    public static boolean getMasterPassword(){
        if(! Preferences.userRoot().getBoolean("usePassword", false)){
            masterPassword = hash(""); //password is empty string
            return true;
        }
        while(true) {
             JPanel panel = new JPanel();
             JLabel label = null;
             label = new JLabel("Enter the master password:");
             JPasswordField pass = new JPasswordField(20);
             panel.add(label);
             panel.add(pass);

             String[] options = new String[]{"OK", "Cancel"};
             int option = JOptionPane.showOptionDialog(null, panel, "Input Master Password",
                     JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                     null, options, options[0]);
             if (option == 0) // pressing OK button
             {
                 masterPassword = hash(new String(pass.getPassword()));
                 //test password
                 if (!decrypt(Preferences.userRoot().getByteArray("passwordCheck", null)).equals(passwordCheck)) {
                     System.out.println("Master password incorrect. retrying.");
                     continue;
                 } else {
                     return true;
                 }
             } else {
                 return false;
             }
         }
    }
    public static boolean setNewMasterPassword(){
    while(true){
            JPanel panel = new JPanel();
            JLabel label = null;
            label = new JLabel("Enter a new master password: (Minimum length 5, or leave blank to disable)");
            JPasswordField pass = new JPasswordField(20);

            panel.add(label);
            panel.add(pass);

            String[] options = new String[]{"OK", "Cancel"};
            int option = JOptionPane.showOptionDialog(null, panel, "Set New Master Password",
                    JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
            if(option == 0  ) {// pressing OK button
                if(pass.getPassword().length >= 5) {
                    //password is valid
                    masterPassword = hash(new String(pass.getPassword()));
                    Preferences.userRoot().putByteArray("passwordCheck", encrypt(passwordCheck));
                    Preferences.userRoot().putBoolean("usePassword", true);
                    return true;
                }else if(pass.getPassword().length >0) {
                    JOptionPane.showMessageDialog(null,new JPanel(), "Error, Password too short", JOptionPane.ERROR_MESSAGE);
                    continue;
                }else{
                    Preferences.userRoot().putBoolean("usePassword", false);
                    masterPassword = hash(""); //password is empty string
                    Preferences.userRoot().putByteArray("passwordCheck", encrypt(passwordCheck));
                    return true;
                }
            }else {
                return false;
            }
    }
    }

    public static String decrypt(byte[] content) {
        SecretKeySpec key = new SecretKeySpec(masterPassword, "Blowfish");

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("Blowfish");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        // initialize for decrypting
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        byte[] decrypted = new byte[0];
        try {
            decrypted = cipher.doFinal(content);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return new String(decrypted);

    }

    public static byte[] encrypt(String data){
        if (masterPassword == null){
            //get master password!
            if (!getMasterPassword()){
                return null;
            }
        }
        SecretKeySpec key = new SecretKeySpec(masterPassword, "Blowfish");

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("Blowfish");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

// initialize for encrypting
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        try {
            return cipher.doFinal(data.getBytes());
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
    /*
            SecretKeySpec key = new SecretKeySpec("MyKey".getBytes(), "Blowfish");

            Cipher cipher = Cipher.getInstance("Blowfish");

// initialize for encrypting
            cipher.init(Cipher.ENCRYPT_MODE, key);

            String toEncrypt = "Encrypt this";
            byte[] encrypted = cipher.doFinal(toEncrypt.getBytes());

// initialize for decrypting
            cipher.init(Cipher.DECRYPT_MODE, key);

            byte[] decrypted = cipher.doFinal(encrypted);
            String afterEncryption = new String(decrypted);
*/


}


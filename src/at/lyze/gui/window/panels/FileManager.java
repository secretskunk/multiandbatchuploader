package at.lyze.gui.window.panels;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.cellEditor.ButtonCellEditor;
import at.lyze.gui.window.cellEditor.EnableControllEditor;
import at.lyze.gui.window.dialogs.EditFileInformationDialog;
import at.lyze.gui.window.listener.FileManagerDragAndDropListener;
import at.lyze.gui.window.listener.UploadAllListener;
import at.lyze.gui.window.listener.UploadListener;
import at.lyze.gui.window.models.EnableControl;
import at.lyze.gui.window.models.FileManagerModel;
import at.lyze.gui.window.renderer.FileManagerProgressRenderer;
import at.lyze.gui.window.tableCellRenderer.ColorEnableControlTableCellRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * Created by lyze on 18.09.15.
 */
public class FileManager extends JPanel {

    private JScrollPane scroller;
    private JTable table;
    private FileManagerModel model;

    private DropTarget dropTarget;

    private UploadListener listener;
    private ButtonCellEditor uploadButtonEditor;

    private JButton uploadAll;

    private boolean currentUploading = false;

    public FileManager(FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser) throws Exception {

        model = new FileManagerModel();
        model.setColumnIdentifiers(new String[]{"Number", "Path", "Name", "Global Settings", "FurAffinity", "So Furry", "Weasyl", "Progress", "ArtworkObject", "Upload"});
        table = new JTable(model);
        table.setDragEnabled(false);
        table.setRowSelectionAllowed(false);
        table.setDefaultRenderer(EnableControl.class,new ColorEnableControlTableCellRenderer());
        table.setDefaultEditor(EnableControl.class, new EnableControllEditor());
        table.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                if (me.getClickCount() == 2) {
                    int row = table.rowAtPoint(p);
                    int col = table.columnAtPoint(p);
                    editRow(row, col);
                }
            }
        });
        table.getColumnModel().getColumn(0).setMaxWidth(80);

        table.removeColumn(table.getColumnModel().getColumn(1));
        table.removeColumn(table.getColumnModel().getColumn(7));

        table.getColumnModel().getColumn(2).setMaxWidth(100);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setMaxWidth(100);
        table.getColumnModel().getColumn(5).setMaxWidth(100);
        table.getColumnModel().getColumn(6).setCellRenderer(new FileManagerProgressRenderer());
        scroller = new JScrollPane(table);

        dropTarget = new DropTarget(table, new FileManagerDragAndDropListener(model));
        setDropTarget(dropTarget);

        listener = new UploadListener(faBrowser, soFurryBrowser, weasylBrowser, this);
        uploadButtonEditor = new ButtonCellEditor(table, listener, 7);
        listener.setUploadButtonEditor(uploadButtonEditor);

        uploadAll = new JButton("Upload all");
        uploadAll.addActionListener(new UploadAllListener(listener, this));

        setLayout(new BorderLayout());
        add(scroller, BorderLayout.CENTER);

        add(uploadAll, BorderLayout.SOUTH);

    }

    private void editRow(int row, int column) {
        System.out.println("Opening Edit File Information Dialog for image " + model.getValueAt(row, 2));
        EditFileInformationDialog dialog = new EditFileInformationDialog(this, new File((String) model.getValueAt(row, 1)));

        dialog.addImage();
        Artwork artwork = (Artwork) model.getValueAt(row, 8);
        artwork.path = String.valueOf(model.getValueAt(row, 1));
        dialog.addArtwork(artwork);

        if (column == 0 || column == 1 || column == 6)
            dialog.setPanel(0);
        else
            dialog.setPanel(1);

        dialog.display();

        if (dialog.isShouldSave()) {
            System.out.println("Closing dialog and saving");
            Artwork art = (Artwork) dialog.getReturnObject();
            model.setValueAt(art, row, 8);
            model.setValueAt(!art.isDefault(true), row, 3);
            //check if we can upload to the sites
            String issues = art.readyForFA();
            if(issues != null){
                ((EnableControl)model.getValueAt(row, 4)).disable(issues);
            }else{
                ((EnableControl)model.getValueAt(row, 4)).enable(true);
            }
             issues = art.readyForSF();
            if(issues != null){
                ((EnableControl)model.getValueAt(row, 5)).disable(issues);
            }else{
                ((EnableControl)model.getValueAt(row, 5)).enable(true);
            }
             issues = art.readyForWY();
            if(issues != null){
                ((EnableControl)model.getValueAt(row, 6)).disable(issues);
            }else{
                ((EnableControl)model.getValueAt(row, 6)).enable(true);
            }
        } else {
            System.out.println("Closing dialog w/o saving");
        }
    }

    public JTable getTable() {
        return table;
    }

    @Override
    public DropTarget getDropTarget() {
        return dropTarget;
    }

    public JButton getUploadAllButton() {
        return uploadAll;
    }

    public boolean isCurrentlyUploading() {
        return currentUploading;
    }

    public void setCurrentlyUploading(boolean currentlyUploading) {
        this.currentUploading = currentlyUploading;
    }
}

package at.lyze.gui.window.panels;

import at.lyze.gui.stuff.Element;
import at.lyze.gui.window.table.PropertyTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.Field;

public class PropertyPanel extends JPanel {

    private DefaultTableModel model;
    private PropertyTable table;

    private JScrollPane scroller;

    private Object returnObject;

    public PropertyPanel() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(800, 600));

        model = new DefaultTableModel();
        model.setColumnIdentifiers(new String[]{"path", "Name", "Understandable Name", "Content", "MainClass"});
        table = new PropertyTable();
        table.setModel(model);
        table.getColumnModel().removeColumn(table.getColumnModel().getColumn(4));
        table.getColumnModel().removeColumn(table.getColumnModel().getColumn(1));
        table.getColumnModel().removeColumn(table.getColumnModel().getColumn(0));

        table.setRowHeight(table.getRowHeight() + 6);

        table.setResizable(true, true);

        scroller = new JScrollPane(table);
        add(scroller, BorderLayout.CENTER);
    }

    public void init(Object obj) {
        try {
            Class<?> c = obj.getClass();

            for (Field f : c.getDeclaredFields()) {
                addFieldToTable(f, obj, true);
            }
        } catch (IllegalAccessException x) {
            x.printStackTrace();
        }
    }

    private void addFieldToTable(Field field, Object object, boolean mainClass) throws IllegalArgumentException, IllegalAccessException {

        String annotation = "NaN";

        if (field.isAnnotationPresent(Element.class)) {
            Element anons = field.getAnnotation(Element.class);
            annotation = anons.type() + " " + anons.name();
        }

        if (field.getType().isArray()) {
            model.addRow(new Object[]{object.getClass().getName(), field.getName(), annotation, field.get(object), mainClass});
        }
        if (field.getType().getName().equals("at.lyze.gui.window.table.FixedValue") || field.getType().isPrimitive() || field.getType().getName().equals("java.lang.String")) {
            model.addRow(new Object[]{object.getClass().getName(), field.getName(), annotation, field.get(object), mainClass});
        } else {
            Class<?> c = field.get(object).getClass();
            for (Field f : c.getDeclaredFields()) {
                addFieldToTable(f, field.get(object), false);
            }
        }
    }

    public JTable getTable() {
        return table;
    }

    public DefaultTableModel getModel() {
        return model;
    }
}

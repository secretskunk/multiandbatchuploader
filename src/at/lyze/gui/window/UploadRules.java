package at.lyze.gui.window;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.prefs.Preferences;

/**
 * Created by SECRETSKUNK on 1/3/16.
 */
public class UploadRules {
    private JTextArea faPrepend;
    private JTextArea faAppend;
    private JPanel rulesForm;
    private JPanel faOptions;
    private JPanel sfOptions;
    private JTextArea sfPrepend;
    private JTextArea sfAppend;
    private JTextArea wyPrepend;
    private JTextArea wyAppend;


    public UploadRules() {
        faPrepend.setText( Preferences.userRoot().get("faPrependString",""));
        faPrepend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("faPrependString", faPrepend.getText());
            }
        });
        faAppend.setText( Preferences.userRoot().get("faAppendString",""));
        faAppend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("faAppendString", faAppend.getText());
            }
        });
        sfPrepend.setText( Preferences.userRoot().get("sfPrependString",""));
        sfPrepend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("sfPrependString", sfPrepend.getText());
            }
        });
        sfAppend.setText( Preferences.userRoot().get("sfAppendString",""));
        sfAppend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("sfAppendString", sfAppend.getText());
            }
        });
        wyPrepend.setText( Preferences.userRoot().get("wyPrependString",""));
        wyPrepend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("wyPrependString", wyPrepend.getText());
            }
        });
        wyAppend.setText( Preferences.userRoot().get("wyAppendString",""));
        wyAppend.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Preferences.userRoot().put("wyAppendString", wyAppend.getText());
            }
        });
        JFrame frame = new JFrame("UploadRules");
        frame.setContentPane(this.rulesForm);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}

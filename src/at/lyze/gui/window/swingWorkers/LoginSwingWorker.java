package at.lyze.gui.window.swingWorkers;

import at.lyze.gui.Main;
import at.lyze.gui.websites.everywhere.LoginStatus;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.dialogs.LoginDialog;

import javax.swing.*;
import java.util.prefs.Preferences;
import static at.lyze.gui.util.Encryption.*;

/**
 * Created by lyze on 20.09.15.
 */
public class LoginSwingWorker extends SwingWorker<Void, Void> {

    private FaBrowser faBrowser;
    private SoFurryBrowser soFurryBrowser;
    private WeasylBrowser weasylBrowser;
    private LoginDialog loginDialog;

    public LoginSwingWorker(LoginDialog loginDialog, FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser) {
        this.loginDialog = loginDialog;
        this.faBrowser = faBrowser;
        this.soFurryBrowser = soFurryBrowser;
        this.weasylBrowser = weasylBrowser;
		//I should really refactor all of this.
		try {
			if (loginDialog.rememberUserInfo.isSelected()&&(Preferences.userRoot().getBoolean("userInfoPresent", false) || setNewMasterPassword())) {
					Preferences.userRoot().putByteArray("furAffinityUsername", encrypt(loginDialog.getFaUsername()));
					Preferences.userRoot().putByteArray("furAffinityPassword", encrypt(loginDialog.getFaPassword()));
					Preferences.userRoot().putByteArray("soFurryUsername", encrypt(loginDialog.getSoFurryUsername()));
					Preferences.userRoot().putByteArray("soFurryPassword", encrypt(loginDialog.getSoFurryPassword()));
					Preferences.userRoot().putByteArray("weasylUsername", encrypt(loginDialog.getWeasylUsername()));
					Preferences.userRoot().putByteArray("weasylPassword", encrypt(loginDialog.getWeasylPassword()));
					Preferences.userRoot().putBoolean("userInfoPresent", true);
				} else {
                loginDialog.rememberUserInfo.setSelected(false);
				//purge password store
				Preferences.userRoot().putByteArray("furAffinityUsername", encrypt(""));
				Preferences.userRoot().putByteArray("furAffinityPassword", encrypt(""));
				Preferences.userRoot().putByteArray("soFurryUsername", encrypt(""));
				Preferences.userRoot().putByteArray("soFurryPassword", encrypt(""));
				Preferences.userRoot().putByteArray("weasylUsername", encrypt(""));
				Preferences.userRoot().putByteArray("weasylPassword", encrypt(""));
                    Preferences.userRoot().putBoolean("rememberUserInfo", false);
					Preferences.userRoot().putBoolean("userInfoPresent", false);
					resetMasterPassword();

			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

    @Override
    protected Void doInBackground() throws Exception {
        loginDialog.setButtonEnabled(false);
        loginDialog.setFAEnabled(false);
        loginDialog.setSoFurryEnabled(false);
        loginDialog.setWeasylEnabled(false);

		if (!LoginStatus.FA) {
			loginDialog.setFaBorderText("Fur Affinity ... logging in");
			loginDialog.repaint();
			LoginStatus.FA = faBrowser.login(loginDialog.getFaUsername(), loginDialog.getFaPassword());
			loginDialog.setFAEnabled(!LoginStatus.FA);
			if (LoginStatus.FA)
				loginDialog.setFaBorderText("Fur Affinity ... successfully logged in");
			else
				loginDialog.setFaBorderText("Fur Affinity ... couldn't login");
			loginDialog.repaint();
		}

		if (!LoginStatus.SO) {
			loginDialog.setSoFurryBorderText("So Furry ... logging in");
			loginDialog.repaint();
			LoginStatus.SO = soFurryBrowser.login(loginDialog.getSoFurryUsername(), loginDialog.getSoFurryPassword());
			loginDialog.setSoFurryEnabled(!LoginStatus.SO);
			if (LoginStatus.SO)
				loginDialog.setSoFurryBorderText("So Furry ... successfully logged in");
			else
				loginDialog.setSoFurryBorderText("So Furry ... couldn't login");
			loginDialog.repaint();
		}

		if (!LoginStatus.WE) {
			loginDialog.setWeasylBorderText("Weasyl ... logging in");
			loginDialog.repaint();
			LoginStatus.WE = weasylBrowser.login(loginDialog.getWeasylUsername(), loginDialog.getWeasylPassword());
			loginDialog.setWeasylEnabled(!LoginStatus.WE);
			if (LoginStatus.WE)
				loginDialog.setWeasylBorderText("Weasyl ... successfully logged in");
			else
				loginDialog.setWeasylBorderText("Weasyl ... couldn't login");
			loginDialog.repaint();
		}

        loginDialog.setButtonEnabled(true);

		int neededOkCount = loginDialog.getFaUsername().equals("") ? 0 : 1;
		neededOkCount += loginDialog.getSoFurryUsername().equals("") ? 0 : 1;
		neededOkCount += loginDialog.getWeasylUsername().equals("") ? 0 : 1;

		if (neededOkCount == Main.getBooleanCount(LoginStatus.FA, LoginStatus.WE, LoginStatus.SO)) {
			loginDialog.dispose();
		}

        return null;
    }
}

package at.lyze.gui.window.swingWorkers;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.window.listener.UploadListener;
import at.lyze.gui.window.panels.FileManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * Created by lyze on 27.09.15.
 */
public class UploadAllSwingWorker extends SwingWorker<Void, Void> {

    private FileManager manager;
    private UploadListener listener;

    public UploadAllSwingWorker(UploadListener listener, FileManager manager) {
        this.listener = listener;
        this.manager = manager;
    }

    @Override
    protected Void doInBackground() throws Exception {

        System.out.println("Uploading all");

        manager.getUploadAllButton().setEnabled(false);
        manager.getDropTarget().setActive(false);

        for (int i = manager.getTable().getModel().getRowCount() - 1; i >= 0; i--) {
            String name = new File(((Artwork) manager.getTable().getModel().getValueAt(i, 8)).path).getName();
            System.out.println("Uploading all ... starting upload " + name);
            listener.actionPerformed(new ActionEvent(this, 0, "" + i));
            do {
                System.out.println("Uploading all ... waiting for upload " + name + " to finish");
                Thread.sleep(1000);
            } while (manager.isCurrentlyUploading());

            System.out.println("Uploading all ... uploaded " + name);

            ProgressMonitor cooldownMonitor = new ProgressMonitor(manager, "65 sek Cooldown", "Please wait", 0, 65);
            for (int sleep = cooldownMonitor.getMinimum(); sleep <= cooldownMonitor.getMaximum(); sleep++) {
                System.out.println("Uploading all ... " + sleep + "/" + cooldownMonitor.getMaximum() + " sek cooldown");
                cooldownMonitor.setProgress(sleep);
                Thread.sleep(1000);
            }
        }

        System.out.println("Uploading all ... finished");
        manager.getDropTarget().setActive(true);
        manager.getUploadAllButton().setEnabled(true);

        return null;
    }
}

package at.lyze.gui.window.swingWorkers;

import at.lyze.gui.Main;
import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.cellEditor.ButtonCellEditor;
import at.lyze.gui.window.panels.FileManager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.io.File;

/**
 * Created by lyze on 25.09.15.
 */
public class UploadSwingWorker extends SwingWorker<Void, Void> {


    private ButtonCellEditor uploadButtonEditor;
    private FaBrowser faBrowser;
    private SoFurryBrowser soFurryBrowser;
    private WeasylBrowser weasylBrowser;

    private FileManager manager;

    private Artwork artwork;


    public UploadSwingWorker(FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser, FileManager manager, Artwork artwork, ButtonCellEditor uploadButtonEditor) {
        this.faBrowser = faBrowser;
        this.soFurryBrowser = soFurryBrowser;
        this.weasylBrowser = weasylBrowser;
        this.manager = manager;
        this.artwork = artwork;
        this.uploadButtonEditor = uploadButtonEditor;
    }


    @Override
    protected Void doInBackground() throws Exception {

        System.out.println("Starting upload for " + new File(artwork.path).getName());


        uploadButtonEditor.setButtonEnabled(false);
        manager.setCurrentlyUploading(true);

        int row = 0;

        for (int i = 0; i < manager.getTable().getModel().getRowCount(); i++) {
            if (manager.getTable().getModel().getValueAt(i, 8).equals(artwork)) {
                row = i;
                break;
            }
        }

        double current = 0.0f;
        double nbr = 100.0 / Main.getBooleanCount((boolean) manager.getTable().getModel().getValueAt(row, 4), (boolean) manager.getTable().getModel().getValueAt(row, 5), (boolean) manager.getTable().getModel().getValueAt(row, 6));

        if ((boolean) manager.getTable().getModel().getValueAt(row, 4)) {
            faBrowser.upload(artwork);
            current += nbr;
            manager.getTable().getModel().setValueAt((int) Math.floor(current), row, 7);
        }

        if ((boolean) manager.getTable().getModel().getValueAt(row, 5)) {
            soFurryBrowser.upload(artwork);
            current += nbr;
            manager.getTable().getModel().setValueAt((int) Math.floor(current), row, 7);
        }

        if ((boolean) manager.getTable().getModel().getValueAt(row, 6)) {
            weasylBrowser.upload(artwork);
            current += nbr;
            manager.getTable().getModel().setValueAt((int) Math.floor(current), row, 7);
        }

        System.out.println("Finished uploading " + new File(artwork.path).getName());

        uploadButtonEditor.setButtonEnabled(true);
        ((DefaultTableModel) manager.getTable().getModel()).removeRow(row);
        manager.setCurrentlyUploading(false);

        return null;
    }
}

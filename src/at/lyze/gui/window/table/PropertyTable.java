package at.lyze.gui.window.table;

import at.lyze.gui.window.MouseInputAdapter.TableColumnResizer;
import at.lyze.gui.window.MouseInputAdapter.TableRowResizer;
import at.lyze.gui.window.cellEditor.ArrayTableCellEditor;
import at.lyze.gui.window.cellEditor.TextAreaCellEditor;
import at.lyze.gui.window.tableCellRenderer.ArrayTableCellRenderer;
import at.lyze.gui.window.tableCellRenderer.TextAreaCellRenderer;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class PropertyTable extends JTable {
    private static final long serialVersionUID = 1L;
    protected MouseInputAdapter rowResizer, columnResizer = null;
    private Class<?> editingClass;

    @Override
    public TableCellRenderer getCellRenderer(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
        Class<?> rowClass = getModel().getValueAt(row, modelColumn).getClass();
        if (rowClass.getName().equals("java.lang.Boolean"))
            return getDefaultRenderer(rowClass);
        else if (rowClass.isArray())
            return new ArrayTableCellRenderer();
        else if (rowClass.getName().equals("java.lang.String"))
            return new TextAreaCellRenderer();
        else
            return getDefaultRenderer(rowClass);
    }

    public void setResizable(boolean row, boolean column) {
        if (row) {
            if (rowResizer == null)
                rowResizer = new TableRowResizer(this);
        } else if (rowResizer != null) {
            removeMouseListener(rowResizer);
            removeMouseMotionListener(rowResizer);
            rowResizer = null;
        }
        if (column) {
            if (columnResizer == null)
                columnResizer = new TableColumnResizer(this);
        } else if (columnResizer != null) {
            removeMouseListener(columnResizer);
            removeMouseMotionListener(columnResizer);
            columnResizer = null;
        }
    }

    @Override
    public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        if (getCursor() == TableColumnResizer.resizeCursor || getCursor() == TableRowResizer.resizeCursor)
            return;
        super.changeSelection(row, column, toggle, extend);
    }

    @Override
    public TableCellEditor getCellEditor(int row, int column) {
        editingClass = null;
        int modelColumn = convertColumnIndexToModel(column);
        editingClass = getModel().getValueAt(row, modelColumn).getClass();

        if (editingClass.getName().equals("java.lang.Boolean"))
            return getDefaultEditor(editingClass);
        else if (editingClass.isArray())
            return new ArrayTableCellEditor(new JComboBox<>());
        else if (editingClass.getName().equals("java.lang.String"))
            return new TextAreaCellEditor();
        else
            return getDefaultEditor(editingClass);
    }


    @Override
    public Class<?> getColumnClass(int column) {
        return editingClass != null ? editingClass : super.getColumnClass(column);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column == 1;
    }
}

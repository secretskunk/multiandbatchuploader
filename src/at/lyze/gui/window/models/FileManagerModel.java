package at.lyze.gui.window.models;

import at.lyze.gui.websites.everywhere.LoginStatus;

import javax.swing.table.DefaultTableModel;

/**
 * Created by lyze on 18.09.15.
 */
public class FileManagerModel extends DefaultTableModel {


    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 4)
            return LoginStatus.FA;
        if (column == 5)
            return LoginStatus.SO;
        if (column == 6)
            return LoginStatus.WE;

        return column == 9;
    }
    @Override
    public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
    }
}

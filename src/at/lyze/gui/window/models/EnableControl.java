package at.lyze.gui.window.models;

/**
 * Created by SECRETSKUNK on 1/5/16.
 */
public class EnableControl {
    private String errors = "";
    private EnableControlStatus value = EnableControlStatus.enabledFalse;
    public  EnableControl(boolean val){
    if (val) {
        value = EnableControlStatus.enabledTrue;
    }
    }

    public String getErrors() {
        return errors;
    }

    public void disable(String errors){
        this.errors = errors;
        if(this.value == EnableControlStatus.enabledTrue){
            this.value = EnableControlStatus.disabledTrue;
        }else{
            this.value = EnableControlStatus.disabledFalse;
        }

    }
    public void enable(boolean value){
        if (value) {
            this.value = EnableControlStatus.enabledTrue;
        }
        else{
            this.value = EnableControlStatus.enabledFalse;

        }
    }
    public void enable(){
        if (this.value == EnableControlStatus.disabledTrue) {
            this.value = EnableControlStatus.enabledTrue;
        }
        else{
            this.value = EnableControlStatus.enabledFalse;

        }
    }
    public boolean booleanValue(){
        if(value == EnableControlStatus.enabledTrue || value == EnableControlStatus.enabledFalse){
            return true;
        }
        return false;
    }
    public boolean isEnabled(){
        if (this.value == EnableControlStatus.enabledFalse || this.value == EnableControlStatus.enabledTrue) {
            return true;
        }else{
            return false;
        }

    }
}

package at.lyze.gui.window.dialogs;

import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.MainWindow;
import at.lyze.gui.window.listener.LoginActionListener;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.HashMap;


import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.MessageDigest;

import java.util.prefs.Preferences;

import static at.lyze.gui.util.Encryption.decrypt;

/**
 * Created by lyze on 19.09.15.
 */
public class LoginDialog extends JDialog {

    public JCheckBox rememberUserInfo;
    private HashMap<String, String> argPairs;
    private JTextField furAffinityUsername;
    private JPasswordField furAffinityPassword;
    private TitledBorder furAffinityBorder;
    private JTextField soFurryUsername;
    private JPasswordField soFurryPassword;
    private TitledBorder soFurryBorder;
    private JTextField weasylUsername;
    private JPasswordField weasylPassword;
    private TitledBorder weasylBorder;
    private JButton login;
    private JButton cancel;
    private JPanel contentHolder;

    public LoginDialog(MainWindow mainWindow, FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser, HashMap<String, String> argPairs) {
        super(mainWindow);
        this.argPairs = argPairs;

        setTitle("Login");

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setResizable(false);

        contentHolder = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        {
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints cs = new GridBagConstraints();
            cs.fill = GridBagConstraints.HORIZONTAL;
            cs.gridx = 0;
            cs.gridy = 0;
            cs.gridwidth = 1;
            panel.add(new JLabel("Username: "), cs);

            cs.fill = GridBagConstraints.HORIZONTAL;
            furAffinityUsername = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 0;
            cs.gridwidth = 2;
            panel.add(furAffinityUsername, cs);

            cs.gridx = 0;
            cs.gridy = 1;
            cs.gridwidth = 1;
            panel.add(new JLabel("Password: "), cs);

            furAffinityPassword = new JPasswordField(20);
            cs.gridx = 1;
            cs.gridy = 1;
            cs.gridwidth = 2;
            panel.add(furAffinityPassword, cs);

            furAffinityBorder = BorderFactory.createTitledBorder("Fur Affinity");
            panel.setBorder(furAffinityBorder);
            constraints.gridx = 0;
            constraints.gridy = 0;
            constraints.gridwidth = 1;
            contentHolder.add(panel, constraints);
        }

        {
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints cs = new GridBagConstraints();
            cs.gridx = 0;
            cs.gridy = 2;
            cs.gridwidth = 1;
            panel.add(new JLabel("Username: "), cs);

            soFurryUsername = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 2;
            cs.gridwidth = 2;
            panel.add(soFurryUsername, cs);

            cs.gridx = 0;
            cs.gridy = 3;
            cs.gridwidth = 1;
            panel.add(new JLabel("Password: "), cs);

            soFurryPassword = new JPasswordField(20);
            cs.gridx = 1;
            cs.gridy = 3;
            cs.gridwidth = 2;
            panel.add(soFurryPassword, cs);


            soFurryBorder = BorderFactory.createTitledBorder("SoFurry");
            panel.setBorder(soFurryBorder);
            constraints.gridx = 0;
            constraints.gridy = 1;
            constraints.gridwidth = 1;
            contentHolder.add(panel, constraints);
        }

        {
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints cs = new GridBagConstraints();
            cs.gridx = 0;
            cs.gridy = 4;
            cs.gridwidth = 1;
            panel.add(new JLabel("Username: "), cs);

            weasylUsername = new JTextField(20);
            cs.gridx = 1;
            cs.gridy = 4;
            cs.gridwidth = 2;
            panel.add(weasylUsername, cs);

            cs.gridx = 0;
            cs.gridy = 5;
            cs.gridwidth = 1;
            panel.add(new JLabel("Password: "), cs);

            weasylPassword = new JPasswordField(20);
            cs.gridx = 1;
            cs.gridy = 5;
            cs.gridwidth = 2;
            panel.add(weasylPassword, cs);


            weasylBorder = BorderFactory.createTitledBorder("Weasyl");
            panel.setBorder(weasylBorder);
            constraints.gridx = 0;
            constraints.gridy = 2;
            constraints.gridwidth = 1;
            contentHolder.add(panel, constraints);
        }

        cancel = new JButton("Close");
        cancel.addActionListener(e -> dispose());
        JPanel bp = new JPanel();
        bp.add(cancel);
        boolean checkboxState = false;
            //load in saved checkbox data
        checkboxState = Preferences.userRoot().getBoolean("userInfoPresent", false);
        rememberUserInfo  = new JCheckBox("Remember username/password?",checkboxState );
        bp.add(rememberUserInfo );

        login = new JButton("Login");
        login.addActionListener(new LoginActionListener(this, faBrowser, soFurryBrowser, weasylBrowser));
        bp.add(login);

        getContentPane().add(contentHolder, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);

        setLocationRelativeTo(mainWindow);
    }

    public void display() {

        if (argPairs.containsKey("-fu") || argPairs.containsKey("-su") || argPairs.containsKey("-wu")) {
            furAffinityUsername.setText(argPairs.get("-fu"));
            furAffinityPassword.setText(argPairs.get("-fp"));

            soFurryUsername.setText(argPairs.get("-su"));
            soFurryPassword.setText(argPairs.get("-sp"));

            weasylUsername.setText(argPairs.get("-wu"));
            weasylPassword.setText(argPairs.get("-wp"));

            login.doClick();
        }else if ( 	Preferences.userRoot().getBoolean("userInfoPresent", false)){
            try {
                furAffinityUsername.setText(decrypt(Preferences.userRoot().getByteArray("furAffinityUsername",null)));
                furAffinityPassword.setText(decrypt(Preferences.userRoot().getByteArray("furAffinityPassword", null)));
                soFurryUsername.setText(decrypt(Preferences.userRoot().getByteArray("soFurryUsername", null)));
                soFurryPassword.setText(decrypt(Preferences.userRoot().getByteArray("soFurryPassword", null)));
                weasylUsername.setText(decrypt(Preferences.userRoot().getByteArray("weasylUsername", null)));
                weasylPassword.setText(decrypt(Preferences.userRoot().getByteArray("weasylPassword", null)));
           } catch(Exception e){
            e.printStackTrace();
        }
        }

        pack();
        setVisible(true);
    }

    public String getFaUsername() {
        return furAffinityUsername.getText();
    }

    public String getFaPassword() {
        return new String(furAffinityPassword.getPassword());
    }

    public String getSoFurryUsername() {
        return soFurryUsername.getText();
    }

    public String getSoFurryPassword() {
        return new String(soFurryPassword.getPassword());
    }

    public String getWeasylUsername() {
        return weasylUsername.getText();
    }

    public String getWeasylPassword() {
        return new String(weasylPassword.getPassword());
    }

    public void setFAEnabled(boolean FAEnabled) {
        furAffinityUsername.setEnabled(FAEnabled);
        furAffinityPassword.setEnabled(FAEnabled);
    }

    public void setSoFurryEnabled(boolean soFurryEnabled) {
        soFurryUsername.setEnabled(soFurryEnabled);
        soFurryPassword.setEnabled(soFurryEnabled);
    }

    public void setWeasylEnabled(boolean weasylEnabled) {
        weasylUsername.setEnabled(weasylEnabled);
        weasylPassword.setEnabled(weasylEnabled);
    }

    public void setFaBorderText(String text) {
        furAffinityBorder.setTitle(text);
    }

    public void setSoFurryBorderText(String text) {
        soFurryBorder.setTitle(text);
    }

    public void setWeasylBorderText(String text) {
        weasylBorder.setTitle(text);
    }

    public void setButtonEnabled(boolean buttonEnabled) {
        login.setEnabled(buttonEnabled);
        cancel.setEnabled(buttonEnabled);
    }
}

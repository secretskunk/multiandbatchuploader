package at.lyze.gui.window.dialogs;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.window.panels.PropertyPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by lyze on 19.09.15.
 */
public class EditFileInformationDialog extends JDialog implements WindowListener {

    private File file;

    private JTabbedPane pane;
    private PropertyPanel propertyPanel;

    private JButton ok;
    private JButton cancle;

    private boolean shouldSave = false;

    private Object returnObject;

    public EditFileInformationDialog(Component c, File file) {
        super();
        this.file = file;
        pane = new JTabbedPane();

        setTitle("Edit Information for  " + file.getName());

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModalityType(ModalityType.APPLICATION_MODAL);

        addWindowListener(this);

        setLayout(new BorderLayout());
        pane.setPreferredSize(new Dimension(800, 600));
        add(pane, BorderLayout.CENTER);

        ok = new JButton("Ok");
        ok.addActionListener(e -> {
            shouldSave = true;
            windowClosing(null);
            dispose();
        });

        cancle = new JButton("Cancel");
        cancle.addActionListener(e -> {
            shouldSave = false;
            windowClosing(null);
            dispose();
        });

        JPanel options = new JPanel();
        options.setLayout(new GridLayout(1, 2));
        options.add(cancle);
        options.add(ok);

        add(options, BorderLayout.SOUTH);

        setLocationRelativeTo(c);
    }

    public ImageIcon rescaleImage(File source, int maxHeight, int maxWidth) {
        int newHeight, newWidth;
        int priorHeight = 0, priorWidth = 0;
        BufferedImage image = null;
        ImageIcon sizeImage;

        try {
            image = ImageIO.read(source);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sizeImage = new ImageIcon(image);

        priorHeight = sizeImage.getIconHeight();
        priorWidth = sizeImage.getIconWidth();

        if ((float) priorHeight / (float) priorWidth > (float) maxHeight / (float) maxWidth) {
            newHeight = maxHeight;
            newWidth = (int) (((float) priorWidth / (float) priorHeight) * (float) newHeight);
        } else {
            newWidth = maxWidth;
            newHeight = (int) (((float) priorHeight / (float) priorWidth) * (float) newWidth);
        }


        BufferedImage resizedImg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(image, 0, 0, newWidth, newHeight, null);
        g2.dispose();

        return (new ImageIcon(resizedImg));
    }

    public void addImage() {
        pane.addTab("Image", new JLabel(rescaleImage(file, 600, 800)));
    }

    public void addArtwork(Artwork artwork) {
        propertyPanel = new PropertyPanel();
        propertyPanel.init(artwork);
        pane.addTab("Artwork", propertyPanel);
    }

    public void display() {
        pack();
        setVisible(true);
    }

    public void setPanel(int index) {
        pane.setSelectedIndex(index);
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowClosed(WindowEvent ee) {
        // TODO Auto-generated method stub
    }

    @Override
    public void windowClosing(WindowEvent we) {
        try {
            propertyPanel.getTable().getCellEditor().stopCellEditing();
        } catch (Exception e) {

        }

        HashMap<String, Object> objects = new HashMap<>();
        String main = null;


        for (int i = 0; i < propertyPanel.getModel().getRowCount(); i++) {
            String path = String.valueOf(propertyPanel.getModel().getValueAt(i, 0));
            String name = String.valueOf(propertyPanel.getModel().getValueAt(i, 1));
            Object objValue = propertyPanel.getModel().getValueAt(i, 3);
            String value = String.valueOf(objValue);
            boolean mainClass = Boolean.parseBoolean(String.valueOf(propertyPanel.getModel().getValueAt(i, 4)));

            if (mainClass) {
                main = path;
            }

            try {


                if (!objects.containsKey(path)) {
                    objects.put(path, Class.forName(path).newInstance());
                }

                Object obj = objects.get(path);
                Field field = obj.getClass().getField(name);

                if (field.getType().isPrimitive()) {
                    switch (field.getType().getName()) {
                        case "byte":
                            field.setByte(obj, Byte.parseByte(value));
                            break;
                        case "short":
                            field.setShort(obj, Short.parseShort(value));
                            break;
                        case "int":
                            field.setInt(obj, Integer.parseInt(value));
                            break;
                        case "long":
                            field.setLong(obj, Long.parseLong(value));
                            break;
                        case "float":
                            field.setFloat(obj, Float.parseFloat(value));
                            break;
                        case "double":
                            field.setDouble(obj, Double.parseDouble(value));
                            break;
                        case "char":
                            field.setChar(obj, value.toCharArray()[0]);
                            break;
                        case "boolean":
                            field.setBoolean(obj, Boolean.parseBoolean(value));
                            break;
                        default:
                            break;
                    }
                } else if (field.getType().getName().equals("java.lang.String")) {
                    field.set(obj, value);
                } else if (field.getType().isArray()) {
                    Object arr = Array.newInstance(field.getType().getComponentType(), ((Object[]) objValue).length);

                    for (int j = 0; j < Array.getLength(arr); j++) {
                        Array.set(arr, j, ((Object[]) objValue)[j]);
                    }

                    field.set(obj, arr);
                } else {
                    System.out.println("Unknown Field: " + field.getType().getName());
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (String s : objects.keySet()) {

            Class<?> c = objects.get(s).getClass();
            for (Field f : c.getDeclaredFields()) {
                if (f.getType().isPrimitive() || f.getType().equals("java.lang.String") || f.getType().isArray())
                    continue;

                if (!objects.containsKey(f.getType().getName())) {
                    continue;
                }

                try {
                    f.set(objects.get(s), objects.get(f.getType().getName()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        returnObject = objects.get(main);

    }

    public Object getReturnObject() {
        return returnObject;
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowIconified(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void windowOpened(WindowEvent e) {
        // TODO Auto-generated method stub

    }

    public boolean isShouldSave() {
        return shouldSave;
    }
}

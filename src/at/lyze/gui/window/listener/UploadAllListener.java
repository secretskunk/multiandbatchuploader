package at.lyze.gui.window.listener;

import at.lyze.gui.window.panels.FileManager;
import at.lyze.gui.window.swingWorkers.UploadAllSwingWorker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lyze on 27.09.15.
 */
public class UploadAllListener implements ActionListener {

    private UploadListener listener;
    private FileManager manager;

    public UploadAllListener(UploadListener listener, FileManager manager) {
        this.listener = listener;
        this.manager = manager;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new UploadAllSwingWorker(listener, manager).execute();
    }
}

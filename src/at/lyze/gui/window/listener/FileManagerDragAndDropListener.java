package at.lyze.gui.window.listener;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.everywhere.LoginStatus;
import at.lyze.gui.window.models.EnableControl;

import javax.activation.MimetypesFileTypeMap;
import javax.swing.table.DefaultTableModel;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.*;
import java.io.File;
import java.util.List;

/**
 * Created by lyze on 19.09.15.
 */
public class FileManagerDragAndDropListener implements DropTargetListener {

    private DefaultTableModel model;
    private MimetypesFileTypeMap mimetypesFileTypeMap;

    public FileManagerDragAndDropListener(DefaultTableModel model) {
        this.model = model;
        mimetypesFileTypeMap = new MimetypesFileTypeMap();
        mimetypesFileTypeMap.addMimeTypes("image png tif jpg jpeg bmp gif");
    }

    @Override
    public void drop(DropTargetDropEvent event) {

        event.acceptDrop(DnDConstants.ACTION_COPY);

        Transferable transferable = event.getTransferable();

        DataFlavor[] flavors = transferable.getTransferDataFlavors();

        for (DataFlavor flavor : flavors) {

            try {

                if (flavor.isFlavorJavaFileListType()) {

                    List files = (List) transferable.getTransferData(flavor);

                    for (Object obj : files) {
                        File file = (File) obj;

                        String mimetype= mimetypesFileTypeMap.getContentType(file);
                        System.out.println("Analyzing File " + file.getName() + ". it has the following mimetype: " + mimetype);
                        String type = mimetype.split("/")[0];
                        if(type.equals("image")) {

                            boolean found = false;
                            for (int i = 0; i < model.getRowCount(); i++) {
                                if (model.getValueAt(i, 1).equals(file.getPath())) {
                                    found = true;
                                    System.out.println("Already found " + file.getName() + " ... ignoring");
                                    break;
                                }
                            }

                            if (!found) {
                                System.out.println("Adding " + file.getName() + " to the table");
                                model.addRow(new Object[]{model.getRowCount() + 1, file.getPath(), file.getName(), false,  new EnableControl(LoginStatus.FA), new EnableControl(LoginStatus.SO),new EnableControl(LoginStatus.WE), 0, Artwork.populateArtwork(), "Upload"});
                            }
                        }
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        event.dropComplete(true);
    }

    @Override
    public void dragEnter(DropTargetDragEvent event) {
    }

    @Override
    public void dragOver(DropTargetDragEvent event) {
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent event) {
    }

    @Override
    public void dragExit(DropTargetEvent dte) {

    }

}
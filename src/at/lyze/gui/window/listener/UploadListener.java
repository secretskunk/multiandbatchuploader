package at.lyze.gui.window.listener;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.cellEditor.ButtonCellEditor;
import at.lyze.gui.window.panels.FileManager;
import at.lyze.gui.window.swingWorkers.UploadSwingWorker;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by lyze on 25.09.15.
 */
public class UploadListener extends AbstractAction {

    private FaBrowser faBrowser;
    private SoFurryBrowser soFurryBrowser;
    private WeasylBrowser weasylBrowser;

    private FileManager manager;
    private ButtonCellEditor uploadButtonEditor;

    public UploadListener(FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser, FileManager manager) {
        this.faBrowser = faBrowser;
        this.soFurryBrowser = soFurryBrowser;
        this.weasylBrowser = weasylBrowser;
        this.manager = manager;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int row = Integer.parseInt(e.getActionCommand());

        Artwork artwork = (Artwork) manager.getTable().getModel().getValueAt(row, 8);

        if (artwork == null || !((boolean) manager.getTable().getModel().getValueAt(row, 3))) {
            System.out.println("Can't upload row " + row + " since you haven't edited it");
            return;
        }

        new UploadSwingWorker(faBrowser, soFurryBrowser, weasylBrowser, manager, artwork, uploadButtonEditor).execute();
    }

    public void setUploadButtonEditor(ButtonCellEditor uploadButtonEditor) {
        this.uploadButtonEditor = uploadButtonEditor;
    }
}

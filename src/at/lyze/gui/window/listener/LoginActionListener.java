package at.lyze.gui.window.listener;

import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.dialogs.LoginDialog;
import at.lyze.gui.window.swingWorkers.LoginSwingWorker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by lyze on 20.09.15.
 */
public class LoginActionListener implements ActionListener {

    private FaBrowser faBrowser;
    private SoFurryBrowser soFurryBrowser;
    private WeasylBrowser weasylBrowser;
    private LoginDialog loginDialog;

    public LoginActionListener(LoginDialog loginDialog, FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser) {
        this.loginDialog = loginDialog;
        this.faBrowser = faBrowser;
        this.soFurryBrowser = soFurryBrowser;
        this.weasylBrowser = weasylBrowser;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        LoginSwingWorker sw = new LoginSwingWorker(loginDialog, faBrowser, soFurryBrowser, weasylBrowser);
        sw.execute();
    }
}

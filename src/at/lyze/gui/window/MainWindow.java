package at.lyze.gui.window;

import at.lyze.gui.stuff.JTextAreaOutputStream;
import at.lyze.gui.websites.fa.FaBrowser;
import at.lyze.gui.websites.soFurry.SoFurryBrowser;
import at.lyze.gui.websites.weasyl.WeasylBrowser;
import at.lyze.gui.window.dialogs.LoginDialog;
import at.lyze.gui.window.panels.FileManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lyze on 18.09.15.
 */
public class MainWindow extends JFrame {

    private FileManager manager;

    private JScrollPane scroller;
    private JEditorPane logger;
    private  JMenuBar menuBar;
    private  JMenu optionsMenue;
    private JMenuItem menuItem;
    public MainWindow(FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser) {
        super("Multi and Batch Uploader");
        //do menu bars

        menuBar = new JMenuBar();
        optionsMenue = new JMenu("Options");
        optionsMenue.getAccessibleContext().setAccessibleDescription("Choose how you upload");
        menuBar.add(optionsMenue);
        menuItem = new JMenuItem("Uploading Rules");
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Set up special rules for uploading.");
        menuItem.addActionListener(e1 -> new UploadRules());
        optionsMenue.add(menuItem);
        menuBar.setVisible(true);
        this.setJMenuBar(menuBar);

        try {
            manager = new FileManager(faBrowser, soFurryBrowser, weasylBrowser);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger = new JEditorPane();
        logger.setContentType("text/html");
        logger.setEditable(false);

        HTMLEditorKit kit = new HTMLEditorKit();
        HTMLDocument doc = new HTMLDocument();
        logger.setEditorKit(kit);
        logger.setDocument(doc);
        scroller = new JScrollPane(logger, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.setPreferredSize(new Dimension(0, 200));

        JTextAreaOutputStream out = new JTextAreaOutputStream(logger, kit, doc, "[OUT] ", "black", System.out);
        JTextAreaOutputStream err = new JTextAreaOutputStream(logger, kit, doc, "[ERR] ", "red", System.err);
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    public void init() {
        setLayout(new BorderLayout());
        add(manager, BorderLayout.CENTER);
        add(scroller, BorderLayout.SOUTH);

        setPreferredSize(new Dimension(1280, 720));

        setLocationByPlatform(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        List<Image> favicons = new ArrayList<>();

        try {
            favicons.add(ImageIO.read(MainWindow.class.getResource("/resources/favicon/16x16.png")));
            favicons.add(ImageIO.read(MainWindow.class.getResource("/resources/favicon/32x32.png")));
            favicons.add(ImageIO.read(MainWindow.class.getResource("/resources/favicon/64x64.png")));
            favicons.add(ImageIO.read(MainWindow.class.getResource("/resources/favicon/128x128.png")));
            favicons.add(ImageIO.read(MainWindow.class.getResource("/resources/favicon/256x256.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setIconImages(favicons);
    }

    public void display() {
        pack();
        setVisible(true);
        System.out.println("Window initialized ... ready to use!");
    }

    public void displayLoginPrompt(FaBrowser faBrowser, SoFurryBrowser soFurryBrowser, WeasylBrowser weasylBrowser, HashMap<String, String> argPairs) {
        if (argPairs.get("-d") == null || argPairs.get("-d").equals("false")) {
            LoginDialog login = new LoginDialog(this, faBrowser, soFurryBrowser, weasylBrowser, argPairs);
            login.display();
        }
    }
}

package at.lyze.gui.window.cellEditor;

import javax.swing.*;
import java.awt.*;

public class ArrayTableCellEditor extends DefaultCellEditor {

	private JComboBox<Object> combo;

	public ArrayTableCellEditor(JComboBox<Object> combo) {
		super(combo);

		this.combo = combo;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		DefaultComboBoxModel<Object> m = new DefaultComboBoxModel<Object>((Object[]) value);
		combo.setModel(m);
		return combo;
	}

	@Override
	public Object getCellEditorValue() {

		Object[] elements = new Object[combo.getItemCount()];
		int added = 1;

		elements[0] = combo.getSelectedItem();
		for (int i = 0; i < combo.getItemCount(); i++) {
			if (combo.getItemAt(i) != combo.getSelectedItem()) {
				elements[added++] = combo.getItemAt(i);
			}
		}
		return elements;
	}
}
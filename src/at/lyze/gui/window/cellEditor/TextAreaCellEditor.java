package at.lyze.gui.window.cellEditor;

import javax.swing.*;
import java.awt.*;

/**
 * Created by lyze on 22.09.15.
 */
public class TextAreaCellEditor extends DefaultCellEditor {
    protected JScrollPane scrollpane;
    protected JTextArea textarea;

    public TextAreaCellEditor() {
        super(new JCheckBox());
        scrollpane = new JScrollPane();
        scrollpane.setBorder(BorderFactory.createEmptyBorder());
        textarea = new JTextArea();
        textarea.setBorder(BorderFactory.createEmptyBorder());
        textarea.setLineWrap(true);
        textarea.setFocusTraversalKeysEnabled(true);
        textarea.setWrapStyleWord(true);
        scrollpane.getViewport().add(textarea);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        textarea.setText((String) value);

        return scrollpane;
    }

    public Object getCellEditorValue() {
        return textarea.getText();
    }
}
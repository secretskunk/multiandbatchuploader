package at.lyze.gui.window.cellEditor;

import at.lyze.gui.window.models.EnableControl;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;

/**
 * Created by SECRETSKUNK on 1/5/16.
 */
public class EnableControllEditor extends AbstractCellEditor implements TableCellEditor {
    EnableControl value = null;
    JCheckBox checkBox = new JCheckBox();
    public EnableControllEditor() {
        checkBox.setRequestFocusEnabled(false);
        checkBox.setHorizontalAlignment(JCheckBox.CENTER);
        checkBox.addActionListener(e-> {if(value.isEnabled()) value.enable(checkBox.isSelected());});
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
       this.value = (EnableControl) value;
        return this.checkBox;
    }



    @Override
    public Object getCellEditorValue() {
        return null;
    }
}
package at.lyze.gui.window.tableCellRenderer;

import at.lyze.gui.window.models.EnableControl;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by SECRETSKUNK on 1/5/16.
 */
//edit of the default boolian renderer
public class ColorEnableControlTableCellRenderer extends JCheckBox implements TableCellRenderer, UIResource {
        private static final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

        public ColorEnableControlTableCellRenderer() {
            super();
        setHorizontalAlignment(JLabel.CENTER);
        setBorderPainted(true);
          }


    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
       Color forground = table.getForeground();
        Color background = table.getBackground();

        if (isSelected) {
            forground = table.getSelectionForeground();
            background = table.getSelectionBackground();
          /*  setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
            */
        }

        if (((EnableControl)value).booleanValue()){
            setToolTipText("");
        }else{
            setToolTipText(((EnableControl)value).getErrors());
            forground = forground.darker();
            background = background.darker();
        }

            setForeground(forground);
            setBackground(background);
        setSelected((value != null && ((EnableControl)value).booleanValue()));

        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
        } else {
            setBorder(noFocusBorder);
        }

        return this;
    }
}


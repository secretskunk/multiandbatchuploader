package at.lyze.gui.window.tableCellRenderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class ArrayTableCellRenderer implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		try {
			return new JLabel(String.valueOf(((Object[]) value)[0]));
		} catch (ArrayIndexOutOfBoundsException e) {
			return new JLabel("");
		}
	}

}

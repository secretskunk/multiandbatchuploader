package at.lyze.gui.stuff;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by lyze on 19.09.15.
 */
public class JTextAreaOutputStream extends OutputStream
{
    private HTMLEditorKit kit;
    private HTMLDocument doc;
    private JEditorPane logger;
    private String prefix;

    private OutputStream old;

    private String buffer;
    private String color = "black";

    public JTextAreaOutputStream(JEditorPane logger, HTMLEditorKit kit, HTMLDocument doc, String prefix, String color, OutputStream old)
    {
        this.logger = logger;
        this.old = old;
        this.prefix = prefix;

        if (kit == null)
            throw new IllegalArgumentException ("Destination is null");

        buffer = prefix;
        this.color = color;

        this.kit = kit;
        this.doc = doc;
    }

    @Override
    public void write(int b) throws IOException
    {
        char c = (char) b;
        String value = Character.toString(c);
        buffer += value;
        if (value.equals("\n")) {
            try {
                kit.insertHTML(doc, doc.getLength(), "<font color='" + color + "'>" + buffer + "</font>", 0, 0, null);
                logger.setCaretPosition(doc.getLength());
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
            buffer = prefix;
        }
        old.write(c);
    }

}

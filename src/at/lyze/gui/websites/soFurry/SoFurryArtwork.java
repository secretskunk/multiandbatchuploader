package at.lyze.gui.websites.soFurry;

import at.lyze.gui.stuff.Element;

import java.util.Arrays;

/**
 * Created by lyze on 25.07.15.
 */
public class SoFurryArtwork {

	@Element(type = "So Furry Artwork", name = "Folder")
	public String[] folder;

	@Element(type = "So Furry Artwork", name = "Appropriate Audience")
	public String[] audienceAppropriate;

	@Element(type = "So Furry Artwork", name = "Visibility")
	public String[] whoCanSee;

	@Override
	public String toString() {
		return "SoFurryArtwork [folder=" + Arrays.toString(folder) + ", audienceAppropriate="
				+ Arrays.toString(audienceAppropriate) + ", whoCanSee=" + Arrays.toString(whoCanSee) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(audienceAppropriate);
		result = prime * result + Arrays.hashCode(folder);
		result = prime * result + Arrays.hashCode(whoCanSee);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SoFurryArtwork other = (SoFurryArtwork) obj;
		if (!Arrays.equals(audienceAppropriate, other.audienceAppropriate))
			return false;
		if (!Arrays.equals(folder, other.folder))
			return false;
		return Arrays.equals(whoCanSee, other.whoCanSee);
	}


}

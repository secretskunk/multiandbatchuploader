package at.lyze.gui.websites.soFurry;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.everywhere.Browser;
import at.lyze.gui.websites.everywhere.HardcodedArtworkSettings;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Created by lyze on 25.07.15.
 */
public class SoFurryBrowser extends Browser {

    public SoFurryBrowser() {
        super("soFurry");
    }

    @Override
    public boolean login(String user, String password) {
        if (user.equals("") || password.equals(""))
            return false;
        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/user/login");

            HtmlForm loginForm = page.getForms().get(1);

            HtmlTextInput username = loginForm.getInputByName("LoginForm[sfLoginUsername]");
            username.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("LoginForm[sfLoginPassword]");
            pass.setText(password);

            HtmlSubmitInput submitInput = loginForm.getInputByValue("Login");
            page = submitInput.click();

            if (page.asText().contains("Please fix the following input errors")) {
                System.err.println("[SoFurry] Wrong username / password");
                return false;
            } else {
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {

        System.out.println("Uploading to So furry");
        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/upload/details?contentType=1");

            HtmlForm uploadForm = (HtmlForm) page.getElementById("sf_uploadform");

            HtmlTextInput title = uploadForm.getInputByName("UploadForm[P_title]");
            title.setText(artwork.title);

            HtmlFileInput fileInput = (HtmlFileInput) page.getElementById("UploadForm_binarycontent");
            fileInput.setValueAttribute(artwork.path);

            HtmlSelect folder = uploadForm.getSelectByName("UploadForm[folderId]");
            boolean set = false;
            for (HtmlOption option : folder.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.so.folder[0].toLowerCase())) {
                    option.setSelected(true);
                    set = true;
                    break;
                }
            }
            if (!set) {
                HtmlTextInput newfolder = uploadForm.getInputByName("UploadForm[newFolderName]");
                newfolder.setText(artwork.so.folder[0]);
            }

            List<HtmlInput> ratings = uploadForm.getInputsByName("UploadForm[contentLevel]");
            for (HtmlInput rating : ratings) {
                if (rating.getValueAttribute().trim().equals("" + HardcodedArtworkSettings.so_audience.get(artwork.so.audienceAppropriate[0]))) {
                    rating.setChecked(true);
                }
            }

            List<HtmlInput> whoCanSee = uploadForm.getInputsByName("UploadForm[P_hidePublic]");
            for (HtmlInput who : whoCanSee) {
                if (who.getValueAttribute().trim().toLowerCase().equals("" + HardcodedArtworkSettings.so_visibility.get(artwork.so.whoCanSee[0]))) {
                    who.setChecked(true);
                }
            }

            HtmlTextArea description = uploadForm.getTextAreaByName("UploadForm[description]");
            description.setText(Preferences.userRoot().get("sfPrependString", "")+artwork.comment.replaceAll( ":icon(\\w+):", "fa!$1")+ Preferences.userRoot().get("sfAppendString", ""));

            HtmlTextInput tags = uploadForm.getInputByName("UploadForm[formtags]");
            tags.setText(artwork.tag.replace(" ", ", "));

            HtmlSubmitInput upload = uploadForm.getInputByName("yt0");
            page = upload.click();

            if (page.getUrl().toString().contains("view")) {
                System.out.println("Uploaded");
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " )");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("https://www.sofurry.com/upload/details?contentType=1");

            HtmlForm uploadForm = (HtmlForm) page.getElementById("sf_uploadform");

            HtmlSelect folder = uploadForm.getSelectByName("UploadForm[folderId]");

            HardcodedArtworkSettings.so_folder.clear();
            for (HtmlOption option : folder.getOptions()) {
                HardcodedArtworkSettings.so_folder.add(option.asText().toLowerCase());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package at.lyze.gui.websites.weasyl;

import at.lyze.gui.stuff.Element;

import java.util.Arrays;

/**
 * Created by lyze on 25.07.15.
 */
public class WeasylArtwork {

    @Element(type = "Weasyl Artwork", name = "Subcategory")
    public String subcategory[];

    @Element(type = "Weasyl Artwork", name = "Folder")
    public String folder[];

    @Element(type = "Weasyl Artwork", name = "Rating")
    public String rating[];

	@Override
	public String toString() {
		return "WeasylArtwork [subcategory=" + Arrays.toString(subcategory) + ", folder=" + Arrays.toString(folder)
				+ ", rating=" + Arrays.toString(rating) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(folder);
		result = prime * result + Arrays.hashCode(rating);
		result = prime * result + Arrays.hashCode(subcategory);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeasylArtwork other = (WeasylArtwork) obj;
		if (!Arrays.equals(folder, other.folder))
			return false;
		if (!Arrays.equals(rating, other.rating))
			return false;
		return Arrays.equals(subcategory, other.subcategory);
	}


}

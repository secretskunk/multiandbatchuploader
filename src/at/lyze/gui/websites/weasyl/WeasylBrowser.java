package at.lyze.gui.websites.weasyl;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.everywhere.Browser;
import at.lyze.gui.websites.everywhere.HardcodedArtworkSettings;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.util.prefs.Preferences;

/**
 * Created by lyze on 25.07.15.
 */
public class WeasylBrowser extends Browser {

    public WeasylBrowser() {
        super("weasyl");
    }

    @Override
    public boolean login(String user, String password) {
        if (user.equals("") || password.equals(""))
            return false;
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/signin");

            HtmlForm loginForm = page.getFormByName("signin");

            HtmlTextInput username = loginForm.getInputByName("username");
            username.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("password");
            pass.setText(password);

            HtmlButton submit = (HtmlButton) loginForm.getByXPath("//*[@id=\"login-box\"]/form/button").get(0);
            page = submit.click();

            if (page.asText().contains("The login information you entered was not correct.")) {
                System.err.println("[Weasyl] Wrong username / password");
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {
        System.out.println("Uploading to Weasyl");
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/submit/visual");

            HtmlForm uploadForm = page.getFormByName("submitvisual");

            HtmlFileInput upload = uploadForm.getInputByName("submitfile");
            upload.setValueAttribute(artwork.path);

            HtmlTextInput title = uploadForm.getInputByName("title");
            title.setText(artwork.title);

            HtmlSelect category = uploadForm.getSelectByName("subtype");
            for (HtmlOption option : category.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.subcategory[0].toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect folder = uploadForm.getSelectByName("folderid");
            for (HtmlOption option : folder.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.folder[0].toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlSelect rating = uploadForm.getSelectByName("rating");
            for (HtmlOption option : rating.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.we.rating[0].toLowerCase())) {
                    option.setSelected(true);
                    break;
                }
            }

            HtmlTextArea description = uploadForm.getTextAreaByName("content");
            description.setText( Preferences.userRoot().get("wyPrependString", "")+artwork.comment.replaceAll(":icon(\\w+):", "$1 (FurAffinity)")+ Preferences.userRoot().get("wyAppendString", ""));

            HtmlTextArea tags = uploadForm.getTextAreaByName("tags");
            tags.setText(artwork.tag.replace(" ", ", "));

            HtmlButton submit = (HtmlButton) page.getByXPath("//*[@id=\"submit-form\"]/div[6]/button").get(0);

            page = submit.click();

            if (page.getUrl().toString().contains("submission")) {
                System.out.println("Uploaded");
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " )");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("https://www.weasyl.com/submit/visual");

            HtmlForm uploadForm = page.getFormByName("submitvisual");

            HtmlSelect category = uploadForm.getSelectByName("subtype");
            HardcodedArtworkSettings.we_category.clear();
            for (HtmlOption option : category.getOptions()) {
                HardcodedArtworkSettings.we_category.add(option.asText().toLowerCase());
            }

            HtmlSelect folder = uploadForm.getSelectByName("folderid");
            HardcodedArtworkSettings.we_folderid.clear();
            for (HtmlOption option : folder.getOptions()) {
                HardcodedArtworkSettings.we_folderid.add(option.asText().toLowerCase());
            }

            HtmlSelect rating = uploadForm.getSelectByName("rating");
            HardcodedArtworkSettings.we_rating.clear();
            for (HtmlOption option : rating.getOptions()) {
                HardcodedArtworkSettings.we_rating.add(option.asText().toLowerCase());
            }


            HardcodedArtworkSettings.we_category.remove(" ");
            HardcodedArtworkSettings.we_rating.remove(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

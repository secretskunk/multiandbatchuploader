package at.lyze.gui.websites.fa;

import at.lyze.gui.stuff.Element;

import java.util.Arrays;

/**
 * Created by lyze on 22.07.15.
 */
public class FaArtwork {

	@Element(type = "FurAffinity Artwork", name = "Category")
	public String[] submissionCategory;

	@Element(type = "FurAffinity Artwork", name = "Theme")
	public String[] submissionTheme;

	@Element(type = "FurAffinity Artwork", name = "Species")
	public String[] species;

	@Element(type = "FurAffinity Artwork", name = "Gender")
	public String[] gender;

	@Element(type = "FurAffinity Artwork", name = "Rating")
	public String[] submissionRating;

	@Element(type = "FurAffinity Artwork", name = "Scraps")
	public boolean scraps = false;

    public FaArtwork() {
        super();
    }

	@Override
	public String toString() {
		return "FaArtwork [submissionCategory=" + Arrays.toString(submissionCategory) + ", submissionTheme="
				+ Arrays.toString(submissionTheme) + ", species=" + Arrays.toString(species) + ", gender="
				+ Arrays.toString(gender) + ", submissionRating=" + Arrays.toString(submissionRating) + ", scraps="
				+ scraps + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(gender);
		result = prime * result + (scraps ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(species);
		result = prime * result + Arrays.hashCode(submissionCategory);
		result = prime * result + Arrays.hashCode(submissionRating);
		result = prime * result + Arrays.hashCode(submissionTheme);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FaArtwork other = (FaArtwork) obj;
		if (!Arrays.equals(gender, other.gender))
			return false;
		if (scraps != other.scraps)
			return false;
		if (!Arrays.equals(species, other.species))
			return false;
		if (!Arrays.equals(submissionCategory, other.submissionCategory))
			return false;
		if (!Arrays.equals(submissionRating, other.submissionRating))
			return false;
		return Arrays.equals(submissionTheme, other.submissionTheme);
	}


}

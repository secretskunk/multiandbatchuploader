package at.lyze.gui.websites.fa;

import at.lyze.gui.websites.everywhere.Artwork;
import at.lyze.gui.websites.everywhere.Browser;
import at.lyze.gui.websites.everywhere.HardcodedArtworkSettings;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Created by lyze on 22.07.15.
 */
public class FaBrowser extends Browser {

    public FaBrowser() {
        super("FurAffinity");
    }

    @Override
    public boolean login(String user, String password) {
        if (user.equals("") || password.equals(""))
            return false;
        try {
            HtmlPage page = client.getPage("https://www.furaffinity.net/login/");
            HtmlForm loginForm = page.getForms().get(0);

            HtmlTextInput name = loginForm.getInputByName("name");
            name.setText(user);

            HtmlPasswordInput pass = loginForm.getInputByName("pass");
            pass.setText(password);

            HtmlSubmitInput loginButton = loginForm.getInputByName("login");
            page = loginButton.click();

            if (page.asText().contains("You have typed in an erroneous username or password")) {
                System.err.println("[FurAffinity] Wrong username / password");
                return false;
            } else
                return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean upload(Artwork artwork) {

        System.out.println("Uploading to FA");

        try {
            HtmlPage page = client.getPage("http://www.furaffinity.net/submit/");

            HtmlForm type = page.getFormByName("myform");
            List<HtmlInput> submissionType = type.getInputsByName("submission_type");

//            for (HtmlInput input : submissionType) {
//                System.out.print(input.getAttribute("value") + ": ");
//                if (input instanceof HtmlRadioButtonInput) {
//                    System.out.println(input.isChecked());
//                }
//            }

            page = type.getInputByValue("Next Step").click();
            HtmlForm upload = page.getFormByName("myform");
            HtmlFileInput fileUpload = upload.getInputByName("submission");
            fileUpload.setValueAttribute(artwork.path);

            page = upload.getInputByValue("Next").click();
            boolean failedToSet = true;
            HtmlForm settings = page.getFormByName("myform");
            HtmlSelect category = settings.getSelectByName("cat");
            for (HtmlOption option : category.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.submissionCategory[0].toLowerCase())) {
                    option.setSelected(true);
                    failedToSet = false;
                    break;                }
            }
            if (failedToSet){
                System.err.println("Couldn't upload to FA: Could not set category");
                return false;
            }
             failedToSet = true;
            HtmlSelect theme = settings.getSelectByName("atype");
            for (HtmlOption option : theme.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.submissionTheme[0].toLowerCase())) {
                    option.setSelected(true);
                    failedToSet = false;
                    break;                }
            }
            if (failedToSet){
                System.err.println("Couldn't upload to FA: Could not set theme");
                return false;
            }
             failedToSet = true;
            HtmlSelect species = settings.getSelectByName("species");
            for (HtmlOption option : species.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.species[0].toLowerCase())) {
                    option.setSelected(true);
                    failedToSet = false;
                    break;                }
            }
            if (failedToSet){
                System.err.println("Couldn't upload to FA: Could not set species");
                return false;
            }
             failedToSet = true;
            HtmlSelect gender = settings.getSelectByName("gender");
            for (HtmlOption option : gender.getOptions()) {
                if (option.asText().trim().toLowerCase().equals(artwork.fa.gender[0].toLowerCase())) {
                    option.setSelected(true);
                    failedToSet = false;
                    break;
                }
            }
            if (failedToSet){
                System.err.println("Couldn't upload to FA: Could not set rating");
                return false;
            }
             failedToSet = true;
            List<HtmlInput> ratings = settings.getInputsByName("rating");
            for (HtmlInput rating : ratings) {
                if (rating.getValueAttribute().trim().toLowerCase().equals("" + HardcodedArtworkSettings.fa_submissionRating.get(artwork.fa.submissionRating[0]))) {
                    rating.setChecked(true);
                    failedToSet = false;
                    break;
                }
            }
            if (failedToSet){
                System.err.println("Couldn't upload to FA: Could not set rating");
                return false;
            }

            HtmlTextInput title = settings.getInputByName("title");
            title.setText(artwork.title);

            HtmlTextArea message = settings.getTextAreaByName("message");
            message.setText( Preferences.userRoot().get("faPrependString","")+artwork.comment+ Preferences.userRoot().get("faAppendString",""));

            HtmlTextArea keywords = settings.getTextAreaByName("keywords");
            keywords.setText(artwork.tag);

            HtmlCheckBoxInput scrap = settings.getInputByName("scrap");
            scrap.setChecked(artwork.fa.scraps);
            page = settings.getInputByName("submit").click();
            if (page.getUrl().toString().contains("/view/")) {
                System.out.println("Uploaded");
                return true;
            } else {
                System.err.println("Couldn't upload picture ( " + page.getUrl() + " )");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void destroy() {
        client.close();
    }

    @Override
    public void fetchOptions() {
        try {
            HtmlPage page = client.getPage("http://www.furaffinity.net/submit/");

            HtmlForm type = page.getFormByName("myform");

            page = type.getInputByValue("Next Step").click();

            HtmlForm upload = page.getFormByName("myform");
            HtmlFileInput fileUpload = upload.getInputByName("submission");
            fileUpload.setValueAttribute(new File("data/tmp.png").getCanonicalPath());

            page = upload.getInputByValue("Next").click();

            // Start here to change the code
            
            HtmlForm settings = page.getFormByName("myform");


            HtmlSelect category = settings.getSelectByName("cat");
            HardcodedArtworkSettings.fa_cat.clear();
            for (HtmlOption option : category.getOptions()) {
                HardcodedArtworkSettings.fa_cat.add(option.asText().toLowerCase());
            }

            HtmlSelect theme = settings.getSelectByName("atype");
            HardcodedArtworkSettings.fa_atype.clear();
            for (HtmlOption option : theme.getOptions()) {
                HardcodedArtworkSettings.fa_atype.add(option.asText().toLowerCase());
            }

            HtmlSelect species = settings.getSelectByName("species");
            HardcodedArtworkSettings.fa_species.clear();
            for (HtmlOption option : species.getOptions()) {
                HardcodedArtworkSettings.fa_species.add(option.asText().toLowerCase());
            }

            HtmlSelect gender = settings.getSelectByName("gender");
            HardcodedArtworkSettings.fa_gender.clear();
            for (HtmlOption option : gender.getOptions()) {
                HardcodedArtworkSettings.fa_gender.add(option.asText().toLowerCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

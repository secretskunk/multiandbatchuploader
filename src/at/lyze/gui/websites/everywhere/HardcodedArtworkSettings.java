package at.lyze.gui.websites.everywhere;

import java.util.ArrayList;
import java.util.HashMap;

public class HardcodedArtworkSettings {
	// fa
	public static ArrayList<String> fa_cat = new ArrayList<>();
	public static ArrayList<String> fa_atype = new ArrayList<>();
	public static ArrayList<String> fa_species = new ArrayList<>();
	public static ArrayList<String> fa_gender = new ArrayList<>();

	public static HashMap<String, Integer> fa_submissionRating = new HashMap<>();

	// so furry
	public static ArrayList<String> so_folder = new ArrayList<>();

	public static HashMap<String, Integer> so_audience = new HashMap<>();
	public static HashMap<String, Integer> so_visibility = new HashMap<>();

	//weasyl
	public static ArrayList<String> we_category = new ArrayList<>();
	public static ArrayList<String> we_folderid = new ArrayList<>();
	public static ArrayList<String> we_rating = new ArrayList<>();


	static {

		// fa
        fa_cat.add("Couldn't login");
        fa_atype.add("Couldn't login");
        fa_species.add("Couldn't login");
        fa_gender.add("Couldn't login");

        fa_submissionRating.put("Couldn't login", 0);
        fa_submissionRating.put("General", 0);
        fa_submissionRating.put("Adult", 1);
		fa_submissionRating.put("Mature", 2);

		// so furry

        so_audience.put("Couldn't login", 0);
        so_audience.put("all ages", 0);
        so_audience.put("adult", 1);
        so_audience.put("extreme", 2);

        so_folder.add("Couldn't login");

        so_visibility.put("Couldn't login",0);
        so_visibility.put("public", 0);
        so_visibility.put("group-only", 1);
		so_visibility.put("friends-only", 2);
		so_visibility.put("hidden", 3);

        // Weasyl
        we_category.add("Couldn't login");
        we_folderid.add("Couldn't login");
        we_rating.add("Couldn't login");
    }
}

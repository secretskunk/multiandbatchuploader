package at.lyze.gui.websites.everywhere;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

/**
 * Created by lyze on 22.07.15.
 */
public abstract class Browser {
    public String name;
    protected WebClient client;

    public Browser(String name) {
        client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setJavaScriptEnabled(false);

        this.name = name;
    }

    public abstract boolean login(String user, String password);

    public abstract boolean upload(Artwork artwork);

    public void destroy() {
        client.close();
    }

    public abstract void fetchOptions();
}

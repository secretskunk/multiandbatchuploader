package at.lyze.gui.websites.everywhere;


import at.lyze.gui.stuff.Element;
import at.lyze.gui.websites.fa.FaArtwork;
import at.lyze.gui.websites.soFurry.SoFurryArtwork;
import at.lyze.gui.websites.weasyl.WeasylArtwork;

/**
 * Created by lyze on 22.07.15.
 */
public class Artwork {

    @Element(type = "General Artwork", name = "Title")
    public String title = "";

    @Element(type = "General Artwork", name = "Comment")
    public String comment = "";

    @Element(type = "General Artwork", name = "Tags (Minimum of 2 required for SF)")
    public String tag = "";

    @Element(type = "General Artwork", name = "Path")
    public String path = "";

    public FaArtwork fa = new FaArtwork();
    public SoFurryArtwork so = new SoFurryArtwork();
    public WeasylArtwork we = new WeasylArtwork();

    public Artwork() {

    }

    public Artwork(String title, String comment, String tag, String path) {
        this.title = title;
        this.comment = comment;
        this.tag = tag;
        this.path = path;
    }

	public static Artwork populateArtwork() {

		Artwork artwork = new Artwork();

		artwork.fa.submissionCategory = HardcodedArtworkSettings.fa_cat.toArray(new String[HardcodedArtworkSettings.fa_cat.size()]);
		artwork.fa.submissionTheme = HardcodedArtworkSettings.fa_atype.toArray(new String[HardcodedArtworkSettings.fa_atype.size()]);
		artwork.fa.species = HardcodedArtworkSettings.fa_species.toArray(new String[HardcodedArtworkSettings.fa_species.size()]);
		artwork.fa.submissionRating = HardcodedArtworkSettings.fa_submissionRating.keySet().toArray(new String[HardcodedArtworkSettings.fa_submissionRating.size()]);
		artwork.fa.gender = HardcodedArtworkSettings.fa_gender.toArray(new String[HardcodedArtworkSettings.fa_gender.size()]);

		artwork.so.folder = HardcodedArtworkSettings.so_folder.toArray(new String[HardcodedArtworkSettings.so_folder.size()]);
		artwork.so.audienceAppropriate = HardcodedArtworkSettings.so_audience.keySet().toArray(new String[HardcodedArtworkSettings.so_audience.size()]);
		artwork.so.whoCanSee = HardcodedArtworkSettings.so_visibility.keySet().toArray(new String[HardcodedArtworkSettings.so_visibility.size()]);

		artwork.we.subcategory = HardcodedArtworkSettings.we_category.toArray(new String[HardcodedArtworkSettings.we_category.size()]);
		artwork.we.folder = HardcodedArtworkSettings.we_folderid.toArray(new String[HardcodedArtworkSettings.we_folderid.size()]);
		artwork.we.rating = HardcodedArtworkSettings.we_rating.toArray(new String[HardcodedArtworkSettings.we_rating.size()]);

		return artwork;
	}

	@Override
	public String toString() {
		return "Artwork [title=" + title + ", comment=" + comment + ", tag=" + tag + ", path=" + path + ", fa=" + fa
				+ ", so=" + so + ", we=" + we + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + ((fa == null) ? 0 : fa.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((so == null) ? 0 : so.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((we == null) ? 0 : we.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artwork other = (Artwork) obj;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (fa == null) {
			if (other.fa != null)
				return false;
		} else if (!fa.equals(other.fa))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (so == null) {
			if (other.so != null)
				return false;
		} else if (!so.equals(other.so))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (we == null) {
			if (other.we != null)
				return false;
		} else if (!we.equals(other.we))
			return false;
		return true;
	}

    public boolean isDefault(boolean ignorePath) {
        Artwork aw = populateArtwork();
        if (ignorePath)
            aw.path = this.path;
        return this.equals(aw);
    }
//determines if this artwork can be uploaded to a given site, so any special checks can be put in place. Returns null if no errors
	public String readyForFA() {
		return null;
	}
	public String readyForSF() {
		if (this.tag.split("//s|,").length <2){
			return "Insufficient Tags to Upload To Sofurry.";
		}
		return null;
	}
	public String readyForWY() {

		return null;
	}

}
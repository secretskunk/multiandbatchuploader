# Downloads #
https://bitbucket.org/secretskunk/multiandbatchuploader/downloads


#How To Use

1. extract the zip file
2. run the .bat (windows) / .sh (*nix) file
3. login
4. wait till the window opens
5. drag and drop the files into it (image png tif jpg jpeg bmp gif)
6. double click the file you want to edit
7. edit it
8. repeat step 6 - 8
9. click either on upload or on upload all
10. wait and do something else

---

#Parameters (All optional... just set them if you are bothered to enter them each time)

* -fu: FurAffinity Username
* -fp FurAffinity Password
* -su So Furry Username 
* -sp So Furry Password
* -wu Weasyl Username 
* -wp Weasyl Password

---

#Requirements:

java 8

---

90% of the code was created by the wise Lyze ... http://www.lyze.at ... michael.weinberger@lyze.at

Source:
https://bitbucket.org/secretskunk/multiandbatchuploader
https://bitbucket.lyze.at/projects/FS/repos/multiandbatchuploader




TODO:
Fix annoying error dialogs with SF upload, so that it reminds user to use at least 2 comma separated tags.

scheduled uploads?

Transition to APIs! (if possible)

Inkbunny and E621.net support